#[deprecated]

//use std::ops::IndexMut;
//use std::ops::Index;
//use crate::AllocatorInterface;
use crate::PAGE_SIZE;


use crate::MAX_PAGES;

#[deprecated]
#[derive(Debug, Copy, Clone)]
pub struct VirtualPtr {
    id: u64,   // Virtual Pointer
    pos: u64,  // Raw Pointer to the beginning
    size: u64, // Size of the allocation
    refe: Option<u64>, // If this Ptr does not OWN the data, but rather refers to another piece of
               // data, you should provide the previous Ptr's id as a Some. pos becomes the offset relative to
               // the previous pointer. The size is bound checked.
}

impl VirtualPtr {
    const fn new(virt_ptr: u64, raw_ptr: u64, size: u64) -> Self {
        Self {
            id: virt_ptr,
            pos: raw_ptr,
            size,
            refe: None,
        }
    }
    const fn refer(virt_ptr: u64, offset: u64, size: u64, refers_to: u64) -> Self {
        Self {
            id: virt_ptr,
            pos: offset,
            size,
            refe: Some(refers_to),
        }
    }
}
#[deprecated]
#[derive(Debug, Clone)]
pub struct VmArena {
    total_size: u64, // The total_size of the arena
    free_ptr: u64,   // The pointer to the first free byte
    memory: Vec<u8>, // Raw Memory
}

impl VmArena {
    fn new(total_size: u64) -> Self {
        let mut memory = Vec::new();
        memory.resize(total_size as usize, 0);
        Self {
            total_size,
            free_ptr: 0,
            memory,
        }
    }
    fn deinit(&mut self) {
        self.free_ptr = 0;
    }

    fn resize(&mut self, new_size: u64) {
        self.total_size = new_size;
        self.memory.resize(new_size as usize, 0);
    }
}

#[deprecated]
#[derive(Debug)]
pub struct VmAllocator {
    page_size: u64,   // Instantiation Constant that defines the size of a page in bytes
    page_number: u64, // Number of Pages for the arenas.
    alloc_blocks: Vec<VirtualPtr>,
    fst_arena: VmArena, // Arena where the allocations are actually made
}

impl VmAllocator {
    fn new(page_size: u64) -> Self {
        let arena = VmArena::new(0);
        Self {
            page_size,
            page_number: 0,
            alloc_blocks: Vec::new(),
            fst_arena: arena,
        }
    }
    fn _add_page(&mut self) {
        self.page_number += 1;
        self.fst_arena.resize(self.page_size * self.page_number);
    }
    fn get_raw_ptr(&self, virt_ptr: u64) -> Option<u64> {
        if let Some(pos) = self.alloc_blocks.iter().position(|x| x.id == virt_ptr) {
            let ptr = self.alloc_blocks[pos];
            let offs = ptr.pos;
            if let Some(refe) = ptr.refe {
                return Some(offs + self.get_raw_ptr(refe)?);
            } else {
                return Some(offs);
            }
        }
        None
    }

    fn get_min_valid_virt_ptr(&self) -> u64 {
        if self.alloc_blocks.is_empty() {
            return 1 + (1 << 63);
        }
        let mut virt_ptrs: Vec<u64> = self
            .alloc_blocks
            .iter()
            .filter(|x| (x.id >> 63) != 0)
            .map(|x| x.id)
            .collect();
        virt_ptrs.sort();
        if let Some(mut res) = virt_ptrs.get(0) {
            for i in 1..virt_ptrs.len() {
                if res + 1 < virt_ptrs[i] {
                    return res + 1;
                } else {
                    res = &virt_ptrs[i];
                }
            }
            if let Some(l) = virt_ptrs.last() {
                return l + 1;
            } else {
                return 1 + (1 << 63);
            }
        } else {
            return 1 + (1 << 63);
        }
    }

    fn collect_garbage(&mut self) {
        self.alloc_blocks.sort_by(|a, b| a.pos.cmp(&b.pos));
        self.fst_arena.deinit();
        for block in self.alloc_blocks.iter_mut() {
            if let Some(_) = block.refe {
                continue;
            }
            for i in 0..block.size {
                self.fst_arena.memory[(self.fst_arena.free_ptr + i) as usize] =
                    self.fst_arena.memory[(block.pos + i) as usize];
            }
            block.pos = self.fst_arena.free_ptr;
            self.fst_arena.free_ptr += block.size;
        }
        if self.page_number > MAX_PAGES {
            self.free_pages();
        }
    }

    fn free_pages(&mut self) {
        let mem_usage =
            (self.fst_arena.free_ptr as f64 - 1.0) / (self.page_size * self.page_number) as f64;
        let max_freeable_pages = (self.page_number - MAX_PAGES) as f64;
        let max_free = (1.0 - mem_usage) * self.page_number as f64;
        self.page_number -= f64::min(max_freeable_pages, max_free) as u64;
        self.fst_arena.resize(self.page_size * self.page_number);
    }

    /// The only way alloc fails is when you run out of system memory and the program panics.
    fn malloc(&mut self, nbytes: u64) -> u64 {
        let mut main_total = self.fst_arena.total_size;
        let mut main_occupied = self.fst_arena.free_ptr;
        let mut free_mem = main_total - main_occupied;
        if free_mem < nbytes as u64 {
            self.collect_garbage();
            main_total = self.fst_arena.total_size;
            main_occupied = self.fst_arena.free_ptr;
            free_mem = main_total - main_occupied;
            let mut i = 0;
            while (free_mem + PAGE_SIZE * i) < (nbytes as u64) {
                i += 1;
            }
            self.page_number += i;
            self.fst_arena.resize(self.page_number * self.page_size);
        }
        let virt_ptr = self.get_min_valid_virt_ptr();
        let new_block = VirtualPtr::new(virt_ptr, self.fst_arena.free_ptr, nbytes as u64);
        self.fst_arena.free_ptr += nbytes as u64;
        self.alloc_blocks.push(new_block);
        virt_ptr
    }

    fn mfree(&mut self, virt_ptr: u64) {
        if let Some(target) = self.alloc_blocks.iter().position(|x| x.id == virt_ptr) {
            self.alloc_blocks.swap_remove(target);
        }
    }

    fn size_of_alloc(&self, virt_ptr: u64) -> Option<u64> {
        if let Some(pos) = self.alloc_blocks.iter().position(|x| x.id == virt_ptr) {
            return Some(self.alloc_blocks[pos].size);
        }
        None
    }
    fn mrefer_to(&mut self, orig_ptr: u64, offset: u64, size: u64) -> u64 {
        if let Some(pos) = self.alloc_blocks.iter().position(|x| x.id == orig_ptr) {
            let ptr = self.alloc_blocks[pos];
            if ptr.size >= (offset + size) {
                let virt_res =
                    VirtualPtr::refer(self.get_min_valid_virt_ptr(), offset, size, orig_ptr);
                self.alloc_blocks.push(virt_res);
                return virt_res.id;
            } else {
                panic!(
                    "{} is smaller than {offset} + {size}, so the reference is invalid!",
                    ptr.size
                );
            }
        }
        panic!("No such ptr {orig_ptr}");
    }
}
//impl Index<usize> for VmAllocator {
//    type Output = u8;
//    fn index(&self, index: usize) -> &Self::Output {
//        self.fst_arena.memory.index(index)
//    }
//}
//impl IndexMut<usize> for VmAllocator {
//    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
//        self.fst_arena.memory.index_mut(index)
//    }
//}


//impl AllocatorInterface for VmAllocator {
//    fn malloc(&mut self, nbytes : u64) -> u64 {
//        self::VmAllocator::malloc(self, nbytes)
//    }
//    fn mfree(&mut self, ptr : u64) {
//        VmAllocator::mfree(self, ptr)
//    }
//    fn mrealloc(&mut self, ptr : u64, size : u64) -> u64 {
//        todo!()
//    }
//}


