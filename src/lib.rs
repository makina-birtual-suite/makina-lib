pub mod blkalloc;
pub mod codegen;
pub mod extension;
pub mod mktox86;
// pub mod vmalloc;
use crate::blkalloc::Indexer;
use ::shellexpand as shexp;
use codegen::MKNBDATA;
use codegen::MKNBEXEC;
use extension::VMXtension;
//use rand::prelude::*;
use std::char::REPLACEMENT_CHARACTER;
use std::collections::binary_heap::*;
use std::io::Write;
use std::sync::{Arc, Mutex, RwLock};

use crate::codegen::get_num_reg_from_asm;
//const STACK_INIT_CAP: usize = 1024;
const PAGE_SIZE: u64 = 64 * 1024;
const MAX_PAGES: u64 = 10;
pub const INST_HALT_VM: u64 =
    0b110_1100_000_000_000_0000000000000000_00000000000000000000000000000000;
pub enum HandleAsm {
    UnresolvedJMP(u64, String),
    UnresolvedCNT(u64, String),
    UnresolvedRef(Vec<u8>),
    Other(u64),
}
// ---- SPECIFICATION ----
// *Bytecode Repr*
//0     ]
//1     |   IMMEDIATE_AREA
//2     |
//3     |
//4     |
//5     |
//6     |
//7     |
//8     |
//9     |
//10    |
//11    |
//12    |
//13    |
//14    |
//15    |
//16    |
//17    |
//18    |
//19    |
//20    |
//21    |
//22    |
//23    |
//24    |
//25    |
//26    |
//27    |
//28    |
//29    |
//30    |
//31    ]
//32
//33
//34
//35
//36
//37
//38
//39                UNDEFINED (Future extension for dual instruction per bytecode instruction)
//40
//41
//42
//43
//44
//45
//46
//47
//48    ]   SRC2 REG:
//49    |   "a 000" "b 001" "c 010" "d 011" "e 100" "f 101" "g 110" "h 111"
//50    ]
//51    ]   SRC1 REG:
//52    |   "a 000" "b 001" "c 010" "d 011" "e 100" "f 101" "g 110" "h 111"
//53    ]
//54    ]   DEST REG:
//55    |   "a 000" "b 001" "c 010" "d 011" "e 100" "f 101" "g 110" "h 111"
//56    ]
//57    > *Int_Sign* bits bits 58-60 Select Conv : "i_to_f_32 000" "i_to_f_64 001" "f_to_i_32 010" "f_to_i_64 011"
//      \ bits 58-60 Select Cmp: "eq 000" "cmp 001" "ez 010"
//      \ bits 58-60 Select BitOp: "and 000" "or 001" "xor 010" "not 011" "shl 100" "shr 101"
//58    ] bits 57-60 Select Load/Store : "load_imm 0000" "load_mem_8 0001" "load_mem_16 0010" "load_mem_32 0011" "load_mem_64 0100" "store_mem_8 0101" "store_mem_16 0110" "store_mem_32 0111" "store_mem_64 1000" "stack_pop 1001" "stack_push 1010" "stack_load 1011" "stack_store 1100"
//59    | bits 58-60 Select Arithm Func: "add 000", "sub 001", "mul 010", "div 011", "mod 100", "101,110,111 are undefined for now, as such Vm will produce error if encountered"
//60    ] bits 58-60 Select Jmp Variant: "jmp 000", "jez 001", "jnz 010", "jge 011", "jlt 100", "cal 101", "hlt 110" "ret 111"
//61    ]
//62    | bits 61-63 Select Inst type from: "Loads/Stores 000", "Arith_int 001", "Arith_Float 010", "Conv 011", "Intcmp 100", "Fltcmp 101", "Jmps 110", "BitwiseOps 111"
//63    ]
//
// *MKNASM Spec*
// Its in the form INST DEST SRC1 SRC2
//
// ----- LOADS/STORE -----
// LOAD IMMEADIATE  :: INST DESTREG u32                                     000_0000_DESTREG_000_000_0000000000000000_IMM(32)
// li       reg val
// LOAD BYTE        :: INST DESTREG u64 (immediate or register for addr)    000_0001_DESTREG_opt(srcreg)_000_0000000000000000_opt(IMM32)
// lb       reg addr
// LOAD QUARTER     :: INST DESTREG u64 (immediate or register for addr)    000_0010_DESTREG_opt(srcreg)_000_0000000000000000_opt(IMM32)
// lq       reg addr
// LOAD HALF        :: INST DESTREG u64 (immediate or register for addr)    000_0011_DESTREG_opt(srcreg)_000_0000000000000000_opt(IMM32)
// lh       reg addr
// LOAD WORD        :: INST DESTREG u64 (immediate or register for addr)    000_0100_DESTREG_opt(srcreg)_000_0000000000000000_opt(IMM32)
// lw       reg addr
// STORE BYTE       :: INST u64 SRC1    (immediate or register for addr)    000_0101_DESTREG_opt(srcreg)_000_0000000000000000_opt(IMM32)
// sb       addr reg
// STORE QUARTER    :: INST u64 SRC1    (immediate or register for addr)    000_0110_DESTREG_opt(srcreg)_000_0000000000000000_opt(IMM32)
// sq       addr reg
// STORE HALF        :: INST u64 SRC1    (immediate or register for addr)    000_0111_DESTREG_opt(srcreg)_000_0000000000000000_opt(IMM32)
// sh       addr reg
// STORE WORD        :: INST u64 SRC1    (immediate or register for addr)    000_1000_DESTREG_opt(srcreg)_000_0000000000000000_opt(IMM32)
// sw       addr reg
// LOAD STACK       :: INST DESTREG SRC1                                    000_1011_DESTREG_src1_000_(...)
// ls       reg reg
// STORE STACK      :: INST DESTREG SRC1                                    000_1100_DESTREG_src1_000_(...)
// ss       reg reg
// NATIVE CALL      :: INST                                                 000_1101_(...)
// ncall            (native call uses the registers as specified on the native call specification)
// ----- ARITH_INT -----
// ADD UNSIGNED INT :: INST DESTREG SRC1 SRC2                               001_0000_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// add_u    reg reg reg
// SUB UNSIGNED INT :: INST DESTREG SRC1 SRC2                               001_0010_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// sub_u    reg reg reg
// MUL UNSIGNED INT :: INST DESTREG SRC1 SRC2                               001_0100_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// mul_u    reg reg reg
// DIV UNSIGNED INT :: INST DESTREG SRC1 SRC2                               001_0110_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// div_u    reg reg reg
// MOD UNSIGNED INT :: INST DESTREG SRC1 SRC2                               001_1000_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// MOD_u    reg reg reg
// ADD SIGNED INT   :: INST DESTREG SRC1 SRC2                               001_0001_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// add_i    reg reg reg
// SUB SIGNED INT   :: INST DESTREG SRC1 SRC2                               001_0011_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// sub_i    reg reg reg
// MUL SIGNED INT   :: INST DESTREG SRC1 SRC2                               001_0101_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// mul_i    reg reg reg
// DIV SIGNED INT   :: INST DESTREG SRC1 SRC2                               001_0111_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// div_i    reg reg reg
// MOD SIGNED INT   :: INST DESTREG SRC1 SRC2                               001_1001_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// mod_i    reg reg reg
// ADD FLOAT        :: INST DESTREG SRC1 SRC2                               001_0001_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// ----- ARITH FLOAT -----
// add_f    reg reg reg
// SUB FLOAT        :: INST DESTREG SRC1 SRC2                               010_001x_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// sub_f    reg reg reg
// MUL FLOAT        :: INST DESTREG SRC1 SRC2                               010_010x_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// mul_f    reg reg reg
// DIV FLOAT        :: INST DESTREG SRC1 SRC2                               010_011x_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// div_f    reg reg reg
// MOD FLOAT        :: INST DESTREG SRC1 SRC2                               010_100x_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// mod_f    reg reg reg
// ----- Conv OPS -----
// INT TO FLOAT 32  :: INST DESTREG SRC1                                    011_000x_DESTREG_SRC1_000_0000000000000000_0(32)
// itof_32  reg reg
// INT TO FLOAT 64  :: INST DESTREG SRC1                                    011_001x_DESTREG_SRC1_000_0000000000000000_0(32)
// itof_64  reg reg
// FLOAT TO INT 32  :: INST DESTREG SRC1                                    011_010x_DESTREG_SRC1_000_0000000000000000_0(32)
// ftoi_32  reg reg
// FLOAT TO INT 64  :: INST DESTREG SRC1                                    011_011x_DESTREG_SRC1_000_0000000000000000_0(32)
// ftoi_64  reg reg
// ----- CMP INT -----
// EQ UNSIGNED INT  :: INST SRC1 SRC2                                       100_0000_000_SRC1_SRC2_0000000000000000_0(32)
// eq_u     reg reg
// CMP UNSIGNED INT :: INST SRC1 SRC2                                       100_0010_000_SRC1_SRC2_0000000000000000_0(32)
// cmp_u     reg reg
// EZ UNSIGNED INT  :: INST SRC1 SRC2                                       100_0100_000_SRC1_SRC2_0000000000000000_0(32)
// ez_u     reg
// EQ SIGNED INT    :: INST SRC1 SRC2                                       100_0001_000_SRC1_SRC2_0000000000000000_0(32)
// eq_i     reg reg
// CMP SIGNED INT   :: INST SRC1 SRC2                                       100_0011_000_SRC1_SRC2_0000000000000000_0(32)
// cmp_i     reg reg
// EZ SIGNED INT    :: INST SRC1 SRC2                                       100_0101_000_SRC1_SRC2_0000000000000000_0(32)
// ez_i     reg
// ----- CMP FLOAT -----
// EQ FLOAT         :: INST SRC1 SRC2                                       101_000x_000_SRC1_SRC2_0000000000000000_0(32)
// eq_f     reg reg
// CMP FLOAT        :: INST SRC1 SRC2                                       101_001x_000_SRC1_SRC2_0000000000000000_0(32)
// cmp_f     reg reg
// EZ FLOAT         :: INST SRC1 SRC2                                       101_011x_000_SRC1_SRC2_0000000000000000_0(32)
// ez_f     reg
// ----- JUMPS -----
// HALT             ::                                                      110_110x_000_000_000_0000000000000000_0(32)
// hlt
// JUMP uncond      :: INST addr(imm or reg)                                110_000x_000_opt(srcreg)_000_0000000000000000_opt(IMM32)
// jmp      addr
// JUMP eq 0        :: INST addr(imm or reg)                                110_001x_000_opt(srcreg)_000_0000000000000000_opt(IMM32)
// jez      addr
// JUMP neq 0       :: INST addr(imm or reg)                                110_010x_000_opt(srcreg)_000_0000000000000000_opt(IMM32)
// jnz      addr
// JUMP >= 0        :: INST addr(imm or reg)                                110_011x_000_opt(srcreg)_000_0000000000000000_opt(IMM32)
// jge      addr
// JUMP  < 0        :: INST addr(imm or reg)                                110_100x_000_opt(srcreg)_000_0000000000000000_opt(IMM32)
// jlt      addr
// CALL             :: INST addr(imm or reg)                                110_101x_000_opt(srcreg)_000_0000000000000000_opt(IMM32)
// call     addr
// RETURN           :: INST                                                 110_111x_000_000_000_0000000000000000_0(32)
// ret      addr
// ----- BITWISE OPS -----
//      \ bits 58-60 Select BitOp: "and 000" "or 001" "xor 010" "not 011" "shl 100" "shr 101"
// AND              :: INST DESTREG SRC1 SRC2                               111_000x_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// and      reg reg reg
// OR               :: INST DESTREG SRC1 SRC2                               111_001x_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// or       reg reg reg
// XOR              :: INST DESTREG SRC1 SRC2                               111_010x_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// xor      reg reg reg
// NOT              :: INST DESTREG SRC1                                    111_011x_DESTREG_SRC1_000_0000000000000000_0(32)
// not      reg reg
// SHL              :: INST DESTREG SRC1 SRC2                               111_100x_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// shl      reg reg reg
// SHL              :: INST DESTREG SRC1 SRC2                               111_101x_DESTREG_SRC1_SRC2_0000000000000000_0(32)
// shl      reg reg reg
// ----- SPEC ENDED -----
// ----- FLAG REGIS -----
// 0 -> Zero Flag (1 if 0, 0 if non-zero)
// 1 -> Gt/lt (1 if gt, 0 if lt)
// 2 -> Eq (1 if eq, 0 if neq)
// 3 ->
// 4 ->
// 5 ->
// 6 ->
// 7 ->
// ----------------------
// ----- NATIVE CALLS -----
// h => select native call.
// a => ARGUMENT 0 (also a return value, as per recomendation)
// b => ARGUMENT 1 (also a return value, as per recomendation)
// c => ARGUMENT 2
// d => ARGUMENT 3
// (...)
// ----- Written Specs -----
// Calling a function pushes the program counter + 1 to the stack (return adress).
// To return from a function, the ret instruction will read special register H.
//

// DONE: Deprecate separate i and u arithmetic insts. Substitute i ops with immediate ops;
// TODO: IMPLEMENT SECTIONS!
// DONE: Implement String literals.
// DONE: Implement variables.
// DONE: Implement string print syscall.

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Process {
    id: u64,
    ptr: u64,
    priority: u32,
}

impl Process {
    fn new(id: u64, ptr: u64, priority: u32) -> Self {
        Self { id, ptr, priority }
    }
}

impl Ord for Process {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.priority.cmp(&self.priority)
    }
}

impl PartialOrd for Process {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

pub trait Loadable {
    fn load(self) -> Vec<u8>;
}

pub trait AllocatorInterface: Indexer<usize, u8> + std::fmt::Debug {
    fn malloc(&mut self, nbytes: u64) -> u64;
    fn mfree(&mut self, ptr: u64);
    fn mrealloc(&mut self, ptr: u64, size: u64) -> u64;
    fn sizealloc(&self, ptr: u64) -> u64;
    fn parentalloc(&self, ptr: u64) -> u64;
}

pub struct VM<T: AllocatorInterface> {
    a: [u8; 8],  // Register A
    b: [u8; 8],  // Register B
    c: [u8; 8],  // Register C
    d: [u8; 8],  // Register D
    e: [u8; 8],  // Register E
    f: [u8; 8],  // Register F
    g: [u8; 8],  // Register G
    sp:[u8; 8],  // Register SP (*Process* Stack Pointer Register)
    flg: u8,     // Flags
    base: usize,
    // stack: Vec<u64>,// Call Stack
    heap: Arc<RwLock<T>>,
    natives: Vec<Box<fn(&mut VM<T>)>>,
    //_program    : Vec<u8>,
    pc: usize,
    hlt: bool,                              // HALT FLAG
    proc_ssize: usize,                      // process stack size
    inst_count: u16, // How many instructions have been executed at this time in the process
    procs: Arc<Mutex<BinaryHeap<Process>>>, // pointers to the process' stack where all the process' information is stored
    // while not executing
    curr_proc: Option<Process>, // the current process in this thread
    minpid: u64,                // Current minimum PID
}

impl<T: AllocatorInterface> VM<T> {
    pub fn new(allocator: Arc<RwLock<T>>, proc_ssize: usize, processes: Arc<Mutex<BinaryHeap<Process>>> ) -> Self {
        Self {
            a: [0; 8],
            b: [0; 8],
            c: [0; 8],
            d: [0; 8],
            e: [0; 8],
            f: [0; 8],
            g: [0; 8],
            sp: [0; 8],
            flg: 0,
            base: 0,
            //stack: Vec::with_capacity(STACK_INIT_CAP),
            heap: allocator,
            natives: Vec::new(),
            //_program: Vec::new(),
            pc: 0,
            hlt: false,
            proc_ssize,
            inst_count: 0,
            procs: processes,
            curr_proc: None,
            minpid: 1,
        }
    }
    pub fn load_prog(&mut self, prog: Vec<u8>, priority: u32) -> u64 {
        let pid = {
            if let Ok(procs) = self.procs.lock() {
                match (self.minpid..)
                    .filter(|x| procs.iter().filter(|z| z.id == *x).count() == 0)
                    .next()
                {
                    Some(x) => {
                        self.minpid = x;
                        x
                    }
                    _ => unreachable!(),
                }
            } else {
                self.minpid
            }
        };
        let (stack, ptr) = {
            let mut heap = self.heap.write().unwrap();
            (
                heap.malloc(self.proc_ssize as u64),
                heap.malloc(prog.len() as u64),
            )
        };
        {
            let mut procs = self.procs.lock().unwrap();
            procs.push(Process::new(pid, stack, priority));
        }
        let heap = self.heap.read().unwrap();
        heap.write_all(stack as usize, &ptr.to_le_bytes());
        heap.write_all(stack as usize + 8, &stack.to_le_bytes());
        heap.write_all(ptr as usize, &prog);
        pid
    }
    pub fn resume(&mut self) {
        self.hlt = false;
        self.execute();
    }
    pub fn load_into_mem(&mut self, _obj: impl Loadable) {
        todo!()
    }
    pub fn push_native(&mut self, f: fn(&mut VM<T>)) {
        self.natives.push(Box::new(f))
    }
    fn get_reg(&mut self, reg: u8) -> &mut [u8; 8] {
        assert!(reg < 8, "ERROR: No such register");
        match reg {
            0 => {
                return &mut self.a;
            }
            1 => {
                return &mut self.b;
            }
            2 => {
                return &mut self.c;
            }
            3 => {
                return &mut self.d;
            }
            4 => {
                return &mut self.e;
            }
            5 => {
                return &mut self.f;
            }
            6 => {
                return &mut self.g;
            }
            7 => {
                return &mut self.sp;
            }
            _ => unreachable!(),
        }
    }
    fn debug(&mut self) {
        println!("{self}")
    }
}
impl<T: AllocatorInterface + std::fmt::Debug> std::fmt::Display for VM<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let reg_a = u64::from_le_bytes(self.a);
        let reg_b = u64::from_le_bytes(self.b);
        let reg_c = u64::from_le_bytes(self.c);
        let reg_d = u64::from_le_bytes(self.d);
        let reg_e = u64::from_le_bytes(self.e);
        let reg_f = u64::from_le_bytes(self.f);
        let reg_g = u64::from_le_bytes(self.g);
        let reg_sp = u64::from_le_bytes(self.sp);
        let reg_flg = u64::from_le_bytes([self.flg, 0, 0, 0, 0, 0, 0, 0]);
        let base = self.base;
        let pc = self.pc;
        let proc = self.procs.lock().unwrap();
        let val = format!(
            "\n
        --------------------------------------------------
        a   : ({reg_a:#020})   {reg_a:#066b}     ({reg_a:#18x})
        b   : ({reg_b:#020})   {reg_b:#066b}     ({reg_b:#18x})
        c   : ({reg_c:#020})   {reg_c:#066b}     ({reg_c:#18x})
        d   : ({reg_d:#020})   {reg_d:#066b}     ({reg_d:#18x})
        e   : ({reg_e:#020})   {reg_e:#066b}     ({reg_e:#18x})
        f   : ({reg_f:#020})   {reg_f:#066b}     ({reg_f:#18x})
        g   : ({reg_g:#020})   {reg_g:#066b}     ({reg_g:#18x})
        sp  : ({reg_sp:#020})   {reg_sp:#066b}     ({reg_sp:#18x})
        flg : ({reg_flg})   {reg_flg:#010b}
        base: ({base})
        pc  : ({pc})
        proc: {proc:?}
        \n
        -----------------------
 
",
        );
        write!(f, "{}", val)
    }
}

pub trait ExecST {
    // Load Instructions
    fn load_flag(&mut self, dest: u8);
    fn load_imm(&mut self, src1: u8, val: u32);
    fn load_upper_imm(&mut self, src1: u8, val: u32);
    fn load_mem_8(&mut self, dest: u8, addr: u64);
    fn load_mem_16(&mut self, dest: u8, addr: u64);
    fn load_mem_32(&mut self, dest: u8, addr: u64);
    fn load_mem_64(&mut self, dest: u8, addr: u64);
    fn store_mem_8(&mut self, dest: u8, addr: u64);
    fn store_mem_16(&mut self, src1: u8, addr: u64);
    fn store_mem_32(&mut self, src1: u8, addr: u64);
    fn store_mem_64(&mut self, src1: u8, addr: u64);
    fn stack_pop(&mut self, dest: u8);
    fn stack_push(&mut self, src1: u8);
    fn native_call(&mut self, imm: u32);
    // Signed Int Add Instructions
    fn add_i(&mut self, dest: u8, src1: u8, imm: u64);
    //fn add_i_imm(&mut self, dest: u8, src1: u8, val: i32);
    // Unsigned Int Add Instructions
    fn add_u(&mut self, dest: u8, src1: u8, src2: u8);
    //fn add_u_imm(&mut self, dest: u8, src1: u8, val: u32);
    // Signed Int Sub Instructions
    fn sub_i(&mut self, dest: u8, src1: u8, imm: u64);
    //fn sub_i_imm(&mut self, dest: u8, src1: u8, val: i32);
    // Unsigned Int Sub Instructions
    fn sub_u(&mut self, dest: u8, src1: u8, src2: u8);
    //fn sub_u_imm(&mut self, dest: u8, src1: u8, val: u32);
    // Signed Int Mul Instructions
    fn mul_i(&mut self, dest: u8, src1: u8, imm: u64);
    //fn mul_i_imm(&mut self, dest: u8, src1: u8, val: i32);
    // Unsigned Int Mul Instructions
    fn mul_u(&mut self, dest: u8, src1: u8, src2: u8);
    //fn mul_u_imm(&mut self, dest: u8, src1: u8, val: u32);
    /// Signed Int Div Instructions
    fn div_i(&mut self, dest: u8, src1: u8, imm: u64);
    //fn div_i_imm(&mut self, dest: u8, src1: u8, val: i32);
    // Unsigned Int Div Instructions
    fn div_u(&mut self, dest: u8, src1: u8, src2: u8);
    //fn div_u_imm(&mut self, dest: u8, src1: u8, val: u32);
    // Signed Int Mod Instructions
    fn mod_i(&mut self, dest: u8, src1: u8, imm: u64);
    //fn mod_i_imm(&mut self, dest: u8, src1: u8, val: i32);
    // Unsigned Int Mod Instructions
    fn mod_u(&mut self, dest: u8, src1: u8, src2: u8);
    //fn mod_u_imm(&mut self, dest: u8, src1: u8, val: u32);
    // Float Add Instructions
    fn add_f(&mut self, dest: u8, src1: u8, src2: u8);
    //fn add_f32_imm(&mut self, dest: u8, src1: u8, val: f32);
    // Float Sub Instructions
    fn sub_f(&mut self, dest: u8, src1: u8, src2: u8);
    //fn sub_f_imm(&mut self, dest: u8, src1: u8, val: f32);
    // Float Mul Instructions
    fn mul_f(&mut self, dest: u8, src1: u8, src2: u8);
    //fn mul_f_imm(&mut self, dest: u8, src1: u8, val: f32);
    // Float Div Instructions
    fn div_f(&mut self, dest: u8, src1: u8, src2: u8);
    //fn div_f_imm(&mut self, dest: u8, src1: u8, val: f32);
    // Float Mod Instructions
    fn mod_f(&mut self, dest: u8, src1: u8, src2: u8);
    //fn mod_f_imm(&mut self, dest: u8, src1: u8, val: f32);
    // Int Float Conv
    fn i_to_f_64(&mut self, dest: u8, src1: u8);
    fn i_to_f_32(&mut self, dest: u8, src1: u8);
    fn f_to_i_64(&mut self, dest: u8, src1: u8);
    fn f_to_i_32(&mut self, dest: u8, src1: u8);
    // Int cmp
    fn eq_i(&mut self, src1: u8, src2: u8);
    fn cmp_i(&mut self, src1: u8, src2: u8);
    fn ez_i(&mut self, src1: u8);
    fn eq_u(&mut self, src1: u8, src2: u8);
    fn cmp_u(&mut self, src1: u8, src2: u8);
    fn ez_u(&mut self, src1: u8);
    fn size_of_mem(&mut self, dest: u8, src1: u8);
    // Float cmp
    fn eq_f(&mut self, src1: u8, src2: u8);
    fn cmp_f(&mut self, src1: u8, src2: u8);
    fn ez_f(&mut self, src1: u8);

    //fn raw(&mut self, dest: u8, src1: u8);
    // Jmps
    fn jez(&mut self, dest: usize);
    fn jnz(&mut self, dest: usize);
    fn jge(&mut self, dest: usize);
    fn jlt(&mut self, dest: usize);
    fn jmp(&mut self, dest: usize);
    fn cal(&mut self, dest: usize); // CALL
    fn hlt(&mut self); // HALT
    fn ret(&mut self); // RETURN
                       // Bitwise Ops
    fn and(&mut self, dest: u8, src1: u8, src2: u8);
    fn or(&mut self, dest: u8, src1: u8, src2: u8);
    fn xor(&mut self, dest: u8, src1: u8, src2: u8);
    fn not(&mut self, dest: u8, src1: u8);
    fn shl(&mut self, dest: u8, src1: u8, src2: u8);
    fn shr(&mut self, dest: u8, src1: u8, src2: u8);
}

impl<T: AllocatorInterface> ExecST for VM<T> {
    /// Loads the flag byte into a register
    fn load_flag(&mut self, dest: u8) {
        assert!(dest < 8, "Registers are 0..7, out of range");
        *self.get_reg(dest) = (self.flg as u64).to_le_bytes();
    }
    ///// Gets a raw, "hardware" pointer, through the "virtual" memory.
    //fn raw(&mut self, dest: u8, src1: u8) {
    //    let src_reg = *self.get_reg(src1);
    //    if let Some(raw_mem) = self.heap.get_raw_ptr(u64::from_le_bytes(src_reg)) {
    //        *self.get_reg(dest) = raw_mem.to_le_bytes();
    //    } else {
    //        eprintln!(
    //            "Virtual Pointer {0:#18x} is not valid! (Use after free?)",
    //            u64::from_le_bytes(*self.get_reg(src1))
    //        );
    //        self.hlt();
    //    }
    //}
    /// Loads immediate into a register
    fn load_imm(&mut self, reg: u8, val: u32) {
        assert!(reg < 8, "Registers are 0..7, out of range");
        *self.get_reg(reg) = (val as i32 as i64 as u64).to_le_bytes();
    }
    fn load_upper_imm(&mut self, reg: u8, val: u32) {
        assert!(reg < 8, "Registers are 0..7, out of range");
        *self.get_reg(reg) = ((val as i32 as i64 as u64) << 32).to_le_bytes();
    }
    fn load_mem_8(&mut self, reg: u8, addr: u64) {
        assert!(reg < 8, "Registers are 0..7, out of range");
        let zeror = self.heap.read().unwrap().read_at(addr as usize);
        *self.get_reg(reg) = [zeror, 0, 0, 0, 0, 0, 0, 0];
    }
    fn load_mem_16(&mut self, reg: u8, addr: u64) {
        assert!(reg < 8, "Registers are 0..7, out of range");
        let mut buf = [0; 8];
        self.heap
            .read()
            .unwrap()
            .read_all(addr as usize..addr as usize + 2, &mut buf);
        *self.get_reg(reg) = buf;
    }
    fn load_mem_32(&mut self, reg: u8, addr: u64) {
        assert!(reg < 8, "Registers are 0..7, out of range");
        let mut buf = [0; 8];
        self.heap
            .read()
            .unwrap()
            .read_all(addr as usize..addr as usize + 4, &mut buf);
        *self.get_reg(reg) = buf;
    }
    fn load_mem_64(&mut self, reg: u8, addr: u64) {
        assert!(reg < 8, "Registers are 0..7, out of range");
        let mut buf = [0; 8];
        self.heap
            .read()
            .unwrap()
            .read_all(addr as usize..addr as usize + 8, &mut buf);
        *self.get_reg(reg) = buf;
    }
    fn store_mem_8(&mut self, reg: u8, addr: u64) {
        assert!(reg < 8, "Registers are 0..7, out of range");
        let tmp = self.get_reg(reg)[0];
        self.heap.read().unwrap().write_at(addr as usize, tmp);
    }
    fn store_mem_16(&mut self, reg: u8, addr: u64) {
        assert!(reg < 8, "Registers are 0..7, out of range");
        let tmp = self.get_reg(reg)[0..2].to_owned();
        self.heap.read().unwrap().write_all(addr as usize, &tmp);
    }
    fn store_mem_32(&mut self, reg: u8, addr: u64) {
        assert!(reg < 8, "Registers are 0..7, out of range");
        let tmp = self.get_reg(reg)[0..4].to_owned();
        self.heap.read().unwrap().write_all(addr as usize, &tmp);
    }
    fn store_mem_64(&mut self, reg: u8, addr: u64) {
        assert!(reg < 8, "Registers are 0..7, out of range");
        let tmp = self.get_reg(reg)[0..8].to_owned();
        self.heap.read().unwrap().write_all(addr as usize, &tmp);
    }
    fn stack_pop(&mut self, dest: u8) {
        assert!(dest < 8, "Registers are 0..7, out of range");
        self.sub_i(get_num_reg_from_asm("sp"), get_num_reg_from_asm("sp"), 8);
        let addr = u64::from_le_bytes(self.sp);
        self.load_mem_64(dest, addr);
    }
    fn stack_push(&mut self, src1: u8) {
        assert!(src1 < 8, "Registers are 0..7, out of range");
        let addr = u64::from_le_bytes(*self.get_reg(get_num_reg_from_asm("sp")));
        self.store_mem_64(src1, addr);
        self.sp = (u64::from_le_bytes(self.sp) + 8).to_le_bytes();
    }
    fn native_call(&mut self, imm: u32) {
        let func = self.natives[imm as usize].as_ref();
        (*func)(self);
    }
    fn add_i(&mut self, dest: u8, src1: u8, imm: u64) {
        assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1)) + imm).to_le_bytes();
    }
    fn add_u(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1))
            + u64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    fn add_f(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1))
            + f64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    //fn add_i_imm(&mut self, dest: u8, src1: u8, val: i32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (i64::from_le_bytes(*self.get_reg(src1)) + val as i64).to_le_bytes();
    //}
    //fn add_u_imm(&mut self, dest: u8, src1: u8, val: u32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1)) + val as u64).to_le_bytes();
    //}
    //fn add_f32_imm(&mut self, dest: u8, src1: u8, val: f32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1)) + val as f64).to_le_bytes();
    //}
    fn sub_i(&mut self, dest: u8, src1: u8, imm: u64) {
        assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1)) - imm).to_le_bytes();
    }
    fn sub_u(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1))
            - u64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    fn sub_f(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1))
            - f64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    //fn sub_i_imm(&mut self, dest: u8, src1: u8, val: i32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (i64::from_le_bytes(*self.get_reg(src1)) - val as i64).to_le_bytes();
    //}
    //fn sub_u_imm(&mut self, dest: u8, src1: u8, val: u32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1)) - val as u64).to_le_bytes();
    //}
    //fn sub_f_imm(&mut self, dest: u8, src1: u8, val: f32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1)) - val as f64).to_le_bytes();
    //}
    fn mul_i(&mut self, dest: u8, src1: u8, imm: u64) {
        assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1)) * imm).to_le_bytes();
    }
    fn mul_u(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1))
            * u64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    fn mul_f(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1))
            * f64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    //fn mul_i_imm(&mut self, dest: u8, src1: u8, val: i32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (i64::from_le_bytes(*self.get_reg(src1)) * val as i64).to_le_bytes();
    //}
    //fn mul_u_imm(&mut self, dest: u8, src1: u8, val: u32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1)) * val as u64).to_le_bytes();
    //}
    //fn mul_f_imm(&mut self, dest: u8, src1: u8, val: f32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1)) * val as f64).to_le_bytes();
    //}
    fn div_i(&mut self, dest: u8, src1: u8, imm: u64) {
        assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1)) / imm).to_le_bytes();
    }
    fn div_u(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1))
            / u64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    fn div_f(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1))
            / f64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    //fn div_i_imm(&mut self, dest: u8, src1: u8, val: i32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (i64::from_le_bytes(*self.get_reg(src1)) / val as i64).to_le_bytes();
    //}
    //fn div_u_imm(&mut self, dest: u8, src1: u8, val: u32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1)) / val as u64).to_le_bytes();
    //}
    //fn div_f_imm(&mut self, dest: u8, src1: u8, val: f32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1)) / val as f64).to_le_bytes();
    //}
    fn mod_i(&mut self, dest: u8, src1: u8, imm: u64) {
        assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1)) % imm).to_le_bytes();
    }
    fn mod_u(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1))
            % u64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    fn mod_f(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1))
            % f64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    //fn mod_i_imm(&mut self, dest: u8, src1: u8, val: i32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (i64::from_le_bytes(*self.get_reg(src1)) % val as i64).to_le_bytes();
    //}
    //fn mod_u_imm(&mut self, dest: u8, src1: u8, val: u32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1)) % val as u64).to_le_bytes();
    //}
    //fn mod_f_imm(&mut self, dest: u8, src1: u8, val: f32) {
    //    assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
    //    *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1)) % val as f64).to_le_bytes();
    //}
    fn jmp(&mut self, dest: usize) {
        self.pc = dest;
    }
    fn jnz(&mut self, dest: usize) {
        let flag = self.flg & 1;
        if flag == 0 {
            self.pc = dest;
        }
    }
    fn jez(&mut self, dest: usize) {
        let flag = self.flg & 1;
        if flag == 1 {
            self.pc = dest;
        }
    }
    fn jge(&mut self, dest: usize) {
        let flag = self.flg & 6;
        if flag == 6 {
            self.pc = dest;
        }
    }
    fn jlt(&mut self, dest: usize) {
        let flag = self.flg & 6;
        if flag == 0 {
            self.pc = dest;
        }
    }
    fn cal(&mut self, dest: usize) {
        let sp = get_num_reg_from_asm("sp");
        let stack_p = u64::from_le_bytes(self.sp) as usize;
        self.heap.read().unwrap().write_all(
            stack_p,
            &[self.base.to_le_bytes(), self.pc.to_le_bytes()].concat(),
        );
        self.base = stack_p;
        self.add_i(sp, sp, 16);
        self.pc = dest;
    }
    fn ret(&mut self) {
        // Getting Return Address from call stack
        let (mut tmp1, mut tmp2) = ([0u8; 8], [0u8; 8]);
        let heap = self.heap.read().unwrap();
        heap.read_all(self.base..self.base + 8, &mut tmp1);
        heap.read_all(self.base + 8..self.base + 16, &mut tmp2);
        self.pc = u64::from_le_bytes(tmp2) as usize;
        self.sp = self.base.to_le_bytes();
        self.base = u64::from_le_bytes(tmp1) as usize;
    }
    fn hlt(&mut self) {
        self.hlt = true;
    }
    fn cmp_u(&mut self, src1: u8, src2: u8) {
        let val1 = u64::from_le_bytes(*self.get_reg(src1));
        let val2 = u64::from_le_bytes(*self.get_reg(src2));
        self.flg = 0;
        if val1 >= val2 {
            self.flg = 6;
        } else {
            self.flg = 0;
            return;
        }
        if val1 != val2 {
            self.flg -= 4;
        }
    }
    fn cmp_i(&mut self, src1: u8, src2: u8) {
        let val1 = i64::from_le_bytes(*self.get_reg(src1));
        let val2 = i64::from_le_bytes(*self.get_reg(src2));
        self.flg = 0;
        if val1 >= val2 {
            self.flg = 6;
        } else {
            self.flg = 0;
            return;
        }
        if val1 != val2 {
            self.flg -= 4;
        }
    }
    fn cmp_f(&mut self, src1: u8, src2: u8) {
        let val1 = f64::from_le_bytes(*self.get_reg(src1));
        let val2 = f64::from_le_bytes(*self.get_reg(src2));
        self.flg = 0;
        if val1 >= val2 {
            self.flg = 6;
        } else {
            self.flg = 0;
            return;
        }
        if val1 != val2 {
            self.flg -= 4;
        }
    }
    fn ez_u(&mut self, src1: u8) {
        let val1 = u64::from_le_bytes(*self.get_reg(src1));
        if val1 == 0 {
            self.flg = 1;
        } else {
            self.flg = 0;
        }
    }
    fn ez_i(&mut self, src1: u8) {
        let val1 = i64::from_le_bytes(*self.get_reg(src1));
        if val1 == 0 {
            self.flg = 1;
        } else {
            self.flg = 0;
        }
    }
    fn ez_f(&mut self, src1: u8) {
        let val1 = f64::from_le_bytes(*self.get_reg(src1));
        if val1 == 0.0 {
            self.flg = 1;
        } else {
            self.flg = 0;
        }
    }
    fn eq_u(&mut self, src1: u8, src2: u8) {
        let val1 = u64::from_le_bytes(*self.get_reg(src1));
        let val2 = u64::from_le_bytes(*self.get_reg(src2));
        self.flg = 0;
        if val1 == val2 {
            self.flg = 4;
        } else {
            self.flg = 0;
        }
    }
    fn eq_i(&mut self, src1: u8, src2: u8) {
        let val1 = i64::from_le_bytes(*self.get_reg(src1));
        let val2 = i64::from_le_bytes(*self.get_reg(src2));
        self.flg = 0;
        if val1 == val2 {
            self.flg = 4;
        } else {
            self.flg = 0;
        }
    }
    fn size_of_mem(&mut self, dest: u8, src1: u8) {
        let ptr = u64::from_le_bytes(*self.get_reg(src1));
        let size = self.heap.read().unwrap().sizealloc(ptr);
        *self.get_reg(dest) = size.to_le_bytes();
    }
    fn eq_f(&mut self, src1: u8, src2: u8) {
        let val1 = f64::from_le_bytes(*self.get_reg(src1));
        let val2 = f64::from_le_bytes(*self.get_reg(src2));
        self.flg = 0;
        if val1 == val2 {
            self.flg = 4;
        } else {
            self.flg = 0;
        }
    }
    fn i_to_f_64(&mut self, dest: u8, src1: u8) {
        *self.get_reg(dest) = (i64::from_le_bytes(*self.get_reg(src1)) as f64).to_le_bytes();
    }
    fn i_to_f_32(&mut self, dest: u8, src1: u8) {
        let mut res = [0; 8];
        let val = (i64::from_le_bytes(*self.get_reg(src1)) as i32 as f32).to_le_bytes();
        for (i, v) in val.into_iter().enumerate() {
            res[i] = v;
        }
        *self.get_reg(dest) = res;
    }
    fn f_to_i_64(&mut self, dest: u8, src1: u8) {
        *self.get_reg(dest) = (f64::from_le_bytes(*self.get_reg(src1)) as i64).to_le_bytes();
    }
    fn f_to_i_32(&mut self, dest: u8, src1: u8) {
        let mut res = [0; 8];
        let val = (i64::from_le_bytes(*self.get_reg(src1)) as i32 as f32).to_le_bytes();
        for (i, v) in val.into_iter().enumerate() {
            res[i] = v;
        }
        *self.get_reg(dest) = res;
    }
    fn or(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1))
            | u64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    fn and(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1))
            & u64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    fn xor(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1))
            ^ u64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    fn not(&mut self, dest: u8, src1: u8) {
        assert!(src1 < 8 && dest < 8, "Registers are 0..7, out of range");
        *self.get_reg(dest) = (!u64::from_le_bytes(*self.get_reg(src1))).to_le_bytes();
    }
    fn shl(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1))
            << u64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
    fn shr(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            src1 < 8 && src2 < 8 && dest < 8,
            "Registers are 0..7, out of range"
        );
        *self.get_reg(dest) = (u64::from_le_bytes(*self.get_reg(src1))
            >> u64::from_le_bytes(*self.get_reg(src2)))
        .to_le_bytes();
    }
}

impl<T: AllocatorInterface> extension::VMXtension for VM<T> {}
impl<T: AllocatorInterface> codegen::Codegen for VM<T> {}

pub trait ExecutorST: ExecST + extension::VMXtension {
    fn execute(&mut self);
}

impl<T: AllocatorInterface> ExecutorST for VM<T> {
    fn execute(&mut self) {
        {
            self.curr_proc = self
                .procs
                .lock()
                .map(|mut x| x.pop())
                .map_err(|e| eprintln!("CRITICAL ERR: {e}"))
                .unwrap();
        }
        self.proc_switch_restore();
        let mut curr_bytes: [u8; 8] = [0; 8];
        let inst_type_mask: u64 = 7 << 61;
        let inst_func_mask: u64 = 15 << 57;
        let dest_regs_mask: u64 = 7 << 54;
        let src1_regs_mask: u64 = 7 << 51;
        let src2_regs_mask: u64 = 7 << 48;
        let xten_code_mask: u64 = (2u64.pow(16) - 1) << 32;
        let mut curr_inst = 0;
        let data_num = u64::from_ne_bytes(crate::codegen::MKNBDATA);
        while let Some(_) = self.curr_proc {
            if curr_inst == data_num {
                break;
            }
            //println!("Program Counter is at {}.", self.pc+;
            //self.debug();
            // let mut trash = String::new();
            // let _meh = std::io::stdin().read_line(&mut trash);
            //std::thread::sleep(std::time::Duration::from_millis(500));
            
            if self.inst_count == 20_000 {
                self.inst_count = 0;
                {
                    let mut procs = self.procs.lock().unwrap();
                    let next = procs.pop();
                    match (self.curr_proc, next) {
                        (Some(x), Some(y)) => {
                            if y.priority >= x.priority {
                                procs.push(y);
                            } else {
                                procs.push(x);
                                self.curr_proc = Some(y);
                            }
                        }
                        _ => {}
                    }
                }
            }
            self.inst_count += 1;
            if self.hlt {
                self.hlt = false;
                self.proc_switch_store();
                self.proc_switch_restore();
                self.inst_count = 0;
            }
            self.heap
                .read()
                .unwrap()
                .read_all(self.pc..self.pc + 8, &mut curr_bytes);
            curr_inst = u64::from_le_bytes(curr_bytes);
            if curr_inst == u64::from_ne_bytes(MKNBEXEC) {
                self.pc += 8;
                continue;
            }
            if curr_inst == u64::from_ne_bytes(MKNBDATA) {
                return;
            }

            let inst_type = ((curr_inst & inst_type_mask) >> 61) as u8;
            let inst_func = ((curr_inst & inst_func_mask) >> 57) as u8;
            let dest_regs = ((curr_inst & dest_regs_mask) >> 54) as u8;
            let src1_regs = ((curr_inst & src1_regs_mask) >> 51) as u8;
            let src2_regs = ((curr_inst & src2_regs_mask) >> 48) as u8;
            let xten_code = ((curr_inst & xten_code_mask) >> 32) as u16;
            let part_immd = curr_inst as u32;
            match inst_type {
                0 => {
                    // Loads/Stores
                    match inst_func {
                        0 => {
                            // Load immediate
                            self.load_imm(dest_regs, part_immd);
                        }
                        14 => {
                            // Load Upper Immediate
                            self.load_upper_imm(dest_regs, part_immd);
                        }
                        1 => {
                            // Load Byte
                            let addr = u64::from_le_bytes(*self.get_reg(src1_regs));
                            self.load_mem_8(dest_regs, addr);
                        }
                        2 => {
                            // Load Quarter
                            let addr = u64::from_le_bytes(*self.get_reg(src1_regs));
                            self.load_mem_16(dest_regs, addr)
                        }
                        3 => {
                            // Load Half
                            let addr = u64::from_le_bytes(*self.get_reg(src1_regs));
                            self.load_mem_32(dest_regs, addr)
                        }
                        4 => {
                            // Load Word
                            let addr = u64::from_le_bytes(*self.get_reg(src1_regs));
                            self.load_mem_64(dest_regs, addr)
                        }
                        5 => {
                            // Store Byte
                            let addr = u64::from_le_bytes(*self.get_reg(dest_regs));
                            self.store_mem_8(src1_regs, addr)
                        }
                        6 => {
                            // Store Quarter
                            let addr = u64::from_le_bytes(*self.get_reg(dest_regs));
                            self.store_mem_16(src1_regs, addr)
                        }
                        7 => {
                            // Store Half
                            let addr = u64::from_le_bytes(*self.get_reg(dest_regs));
                            self.store_mem_32(src1_regs, addr)
                        }
                        8 => {
                            // Store Word
                            let addr = u64::from_le_bytes(*self.get_reg(dest_regs));
                            self.store_mem_64(src1_regs, addr)
                        }
                        9 => {
                            // Stack Pop
                            self.stack_pop(dest_regs);
                        }
                        10 => {
                            // Stack Push
                            self.stack_push(src1_regs);
                        }
                        13 => {
                            // NATIVE CALLS
                            self.native_call(part_immd);
                        }
                        15 => {
                            // EXTENSION HANDLING
                            self.execute_extensions(
                                dest_regs, src1_regs, src2_regs, part_immd, xten_code,
                            );
                        }
                        _ => unreachable!(),
                    }
                }
                1 => {
                    // INT ARITH
                    match inst_func {
                        0 => {
                            // add_u
                            self.add_u(dest_regs, src1_regs, src2_regs);
                        }
                        1 => {
                            // add_i
                            self.add_i(dest_regs, src1_regs, part_immd as u64);
                        }
                        2 => {
                            // sub_u
                            self.sub_u(dest_regs, src1_regs, src2_regs);
                        }
                        3 => {
                            // sub_i
                            self.sub_i(dest_regs, src1_regs, part_immd as u64);
                        }
                        4 => {
                            // mul_u
                            self.mul_u(dest_regs, src1_regs, src2_regs);
                        }
                        5 => {
                            // mul_i
                            self.mul_i(dest_regs, src1_regs, part_immd as u64);
                        }
                        6 => {
                            // div_u
                            self.div_u(dest_regs, src1_regs, src2_regs);
                        }
                        7 => {
                            // div_i
                            self.div_i(dest_regs, src1_regs, part_immd as u64);
                        }
                        8 => {
                            // mod_u
                            self.mod_u(dest_regs, src1_regs, src2_regs);
                        }
                        9 => {
                            // mod_i
                            self.mod_i(dest_regs, src1_regs, part_immd as u64);
                        }

                        _ => unreachable!(),
                    }
                }
                2 => {
                    // FLOAT ARITH
                    match inst_func >> 1 {
                        0 => {
                            // add_f
                            self.add_f(dest_regs, src1_regs, src2_regs);
                        }
                        1 => {
                            // sub_f
                            self.sub_f(dest_regs, src1_regs, src2_regs);
                        }
                        2 => {
                            // mul_f
                            self.mul_f(dest_regs, src1_regs, src2_regs);
                        }
                        3 => {
                            // div_f
                            self.div_f(dest_regs, src1_regs, src2_regs);
                        }
                        4 => {
                            // mod_f
                            self.mod_f(dest_regs, src1_regs, src2_regs);
                        }
                        _ => unreachable!(),
                    }
                }
                3 => {
                    // Conv Ops
                    match inst_func >> 1 {
                        0 => {
                            // itof_32
                            self.i_to_f_32(dest_regs, src1_regs);
                        }
                        1 => {
                            // itof_64
                            self.i_to_f_64(dest_regs, src1_regs);
                        }
                        2 => {
                            // ftoi_32
                            self.f_to_i_32(dest_regs, src1_regs);
                        }
                        3 => {
                            // ftoi_64
                            self.f_to_i_64(dest_regs, src1_regs);
                        }
                        _ => unreachable!(),
                    }
                }
                4 => {
                    // Int Cmp
                    match inst_func {
                        0 => {
                            //eq_u
                            self.eq_u(src1_regs, src2_regs);
                        }
                        1 => {
                            //eq_i
                            self.eq_i(src1_regs, src2_regs);
                        }
                        2 => {
                            //cmp_u
                            self.cmp_u(src1_regs, src2_regs);
                        }
                        3 => {
                            //cmp_i
                            self.cmp_i(src1_regs, src2_regs);
                        }
                        4 => {
                            //ez_u
                            self.ez_u(src1_regs);
                        }
                        5 => {
                            //ez_i
                            self.ez_i(src1_regs);
                        }
                        6 => {
                            // size
                            self.size_of_mem(dest_regs, src1_regs);
                        }
                        _ => unreachable!(),
                    }
                }
                5 => {
                    // Float Cmp
                    match inst_func >> 1 {
                        0 => {
                            // eq_f
                            self.eq_f(src1_regs, src2_regs);
                        }
                        1 => {
                            // cmp_f
                            self.cmp_f(src1_regs, src2_regs);
                        }
                        2 => {
                            // ez_f
                            self.ez_f(src1_regs);
                        }
                        3 => {
                            // raw
                            panic!("Raw instruction has been removed!");
                            // self.raw(dest_regs, src1_regs);
                        }
                        _ => unreachable!(),
                    }
                }
                6 => {
                    // Jumps
                    match inst_func >> 1 {
                        0 => {
                            // jmp
                            let next_pc = (((curr_inst as i32 as i64) + self.pc as i64)
                                & 4_294_967_295) as usize;
                            if src1_regs != 0 {
                                let addr = u64::from_le_bytes(*self.get_reg(src1_regs)) as usize;
                                let offs = part_immd as usize;
                                self.jmp(addr + offs);
                            } else {
                                self.jmp(next_pc);
                            }
                        }
                        1 => {
                            // jez
                            let next_pc = (((curr_inst as i32 as i64) + self.pc as i64)
                                & 4_294_967_295) as usize;
                            if src1_regs != 0 {
                                let addr = u64::from_le_bytes(*self.get_reg(src1_regs)) as usize;
                                let offs = part_immd as usize;
                                self.jez(addr + offs);
                            } else {
                                self.jez(next_pc);
                            }
                        }
                        2 => {
                            // jnz
                            let next_pc = (((curr_inst as i32 as i64) + self.pc as i64)
                                & 4_294_967_295) as usize;
                            if src1_regs != 0 {
                                let addr = u64::from_le_bytes(*self.get_reg(src1_regs)) as usize;
                                let offs = part_immd as usize;
                                self.jnz(addr + offs);
                            } else {
                                self.jnz(next_pc);
                            }
                        }
                        3 => {
                            // jge
                            let next_pc = (((curr_inst as i32 as i64) + self.pc as i64)
                                & 4_294_967_295) as usize;
                            if src1_regs != 0 {
                                let addr = u64::from_le_bytes(*self.get_reg(src1_regs)) as usize;
                                let offs = part_immd as usize;
                                self.jge(addr + offs);
                            } else {
                                self.jge(next_pc);
                            }
                        }
                        4 => {
                            // jlt
                            let next_pc = (((curr_inst as i32 as i64) + self.pc as i64)
                                & 4_294_967_295) as usize;
                            if src1_regs != 0 {
                                let addr = u64::from_le_bytes(*self.get_reg(src1_regs)) as usize;
                                let offs = part_immd as usize;
                                self.jlt(addr + offs);
                            } else {
                                self.jlt(next_pc);
                            }
                        }
                        5 => {
                            // cal
                            let next_pc = (((curr_inst as i32 as i64) + self.pc as i64)
                                & 4_294_967_295) as usize;
                            if src1_regs != 0 {
                                let addr = u64::from_le_bytes(*self.get_reg(src1_regs)) as usize;
                                let offs = part_immd as usize;
                                self.cal(addr + offs);
                            } else {
                                self.cal(next_pc);
                            }
                        }
                        6 => self.hlt(), // HALT
                        7 => self.ret(), // RETURN
                        _ => unreachable!(),
                    }
                }
                7 => {
                    // BitwiseOps
                    match inst_func >> 1 {
                        0 => {
                            // and
                            self.and(dest_regs, src1_regs, src2_regs);
                        }
                        1 => {
                            // or
                            self.or(dest_regs, src1_regs, src2_regs);
                        }
                        2 => {
                            // xor
                            self.xor(dest_regs, src1_regs, src2_regs);
                        }
                        3 => {
                            // not
                            self.not(dest_regs, src1_regs);
                        }
                        4 => {
                            // shl
                            self.shl(dest_regs, src1_regs, src2_regs);
                        }
                        5 => {
                            // shr
                            self.shr(dest_regs, src1_regs, src2_regs);
                        }
                        _ => unreachable!(),
                    }
                }
                _ => unreachable!(),
            }
            self.pc += 8;
        }
    }
}

pub trait NiceDefaults {
    fn alloc(&mut self); // Allocate Memory. By convention,  NATIVE CALL 0, obtains size in bytes from ARGUMENT 0 and returns a pointer in the same register.
    fn free(&mut self); // Free Memory. By convention,      NATIVE CALL 1, obtains pointer with any range of offset of a memory block and deallocs that memory block.
    fn print_char(&mut self); // Print char. By convention,       NATIVE CALL 2, obtains lower u8 in register ARGUMENT 0 and prints it as a char.
    fn print_int(&mut self); // Print signed Integer. b.conv,    NATIVE CALL 3, obtains i64 in register ARGUMENT 0 and prints it
    fn print_uint(&mut self); // Print unsigned Int. b.conv,      NATIVE CALL 4, obtains u64 in register ARGUMENT 0 and prints it
    fn print_f32(&mut self); // PRINT f32.                       NATIVE CALL 5, obtains f32 in register ARGUMENT 0 and prints it
    fn print_f64(&mut self); // PRINT f64,                       NATIVE CALL 6, obtains f64 in register ARGUMENT 0 and prints it
    fn read(&mut self); // READ,                            NATIVE CALL 7, safely allocates memory and reads user input, returning pointer in ARGUMENT 0
    fn print_ascii_str(&mut self); //  NATIVE CALL 9: receives pointer in register ARG0 and size in ARG1 and print it to string.
    fn prog_exit(&mut self); // NATIVE CALL 10: exits the program according with the value on
                             // register A. This exits the makina's process, not the OS's process. Prints a status message
                             // in the terminal.
    fn proc_switch_store(&mut self); // NATIVE CALL 11 : stores all process info onto the stack
                                     // address and steps onto the next process.
    fn proc_switch_restore(&mut self); // NATIVE CALL 12: restores all the process' information
    fn spawn_process(&mut self); // NATIVE CALL 13: spawns a new process which ptr to the file name
                                 // is in register A and size in register B.
    fn file_load(&mut self); // NATIVE CALL 14: opens a file and reads it into memory
    fn load_nice_natives(&mut self);
}

impl<T: AllocatorInterface> NiceDefaults for VM<T> {
    fn alloc(&mut self) {
        // NATIVE CALL 0
        let bytes_n = u64::from_le_bytes(self.a);
        self.a = self.heap.write().unwrap().malloc(bytes_n).to_le_bytes();
    }
    fn free(&mut self) {
        // NATIVE CALL 1
        let ptr = u64::from_le_bytes(self.a);
        self.heap.write().unwrap().mfree(ptr);
        self.a = 0u64.to_le_bytes();
    }
    fn print_char(&mut self) {
        // NATIVE CALL 2
        let val =
            std::char::from_u32(u64::from_le_bytes(self.a) as u32).unwrap_or(REPLACEMENT_CHARACTER);
        print!("{val}");
    }
    fn print_int(&mut self) {
        // NATIVE CALL 3
        let val = i64::from_le_bytes(self.a);
        print!("{val}");
    }
    fn print_uint(&mut self) {
        // NATIVE CALL 4
        let val = u64::from_le_bytes(self.a);
        print!("{val}");
    }
    fn print_f32(&mut self) {
        // NATIVE CALL 5
        let val = f32::from_le_bytes(self.a[..4].try_into().unwrap());
        print!("{val}");
    }
    fn print_f64(&mut self) {
        // NATIVE CALL 6
        let val = f64::from_le_bytes(self.a);
        print!("{val}");
    }
    fn read(&mut self) {
        // NATIVE CALL 7
        let mut str_in = String::new();
        let input = std::io::stdin();
        match input.read_line(&mut str_in) {
            Ok(_) => {
                let mem_c: Vec<u8> = str_in.chars().map(|c| c as u8).collect();
                let ptr = self.heap.write().unwrap().malloc(mem_c.len() as u64);
                self.a = ptr.to_le_bytes();
                self.heap.read().unwrap().write_all(ptr as usize, &mem_c);
            }
            _ => self.read(),
        }
    }
    fn print_ascii_str(&mut self) {
        let ptr = u64::from_le_bytes(self.a) as usize;
        let size = u64::from_le_bytes(self.b) as usize;
        let stdout = std::io::stdout();
        let mut lock = stdout.lock();
        let mut buf = Vec::new();
        buf.resize(size as usize, 0);
        self.heap
            .read()
            .unwrap()
            .read_all(ptr..ptr + size, &mut buf);
        lock.write(&buf)
            .expect("SYSCALL: FAILED TO PRINT ASCII STRING");
        lock.flush().expect("SYSCALL: FAILED TO PRINT ASCII STRING");
    }
    fn prog_exit(&mut self) {
        let status = i64::from_le_bytes(self.a);
        if status != 0 {
            eprintln!("\nProgram exited with status {status}.");
        }
        {
            let mut heap = self.heap.write().unwrap();
            heap.mfree(self.base as u64);
            heap.mfree(self.pc as u64);
        }
        {
            self.curr_proc = self.procs.lock().unwrap().pop();
        }
        match self.curr_proc {
            Some(_) => {
                self.proc_switch_restore();
                self.pc -= 8;
            }
            _ => return,
        }
    }
    fn proc_switch_store(&mut self) {
        let ptr = u64::from_le_bytes(self.sp);
        let idx = ptr as usize;
        // Pc -> Base -> Registers -> Flags
        let mut cat = vec![self.pc.to_le_bytes(), self.base.to_le_bytes()];
        for i in 0..8 {
            cat.push(*self.get_reg(i));
        }
        let mut write = cat.concat();
        write.push(self.flg);
        self.heap.read().unwrap().write_all(idx, &write);

        // advancing to the next process
        {
            if let Ok(mut procs) = self.procs.lock() {
                procs.push(
                    self.curr_proc
                        .map(|mut x| {
                            x.priority += 1;
                            x.ptr = ptr;
                            x
                        })
                        .unwrap(),
                ); // unwrap should
                   // be safe as this can't be called without bytecode calling it
                self.curr_proc = procs.pop();
            }
        }
    }
    fn proc_switch_restore(&mut self) {
        let ptr = self.curr_proc.unwrap().ptr;
        let mut idx = ptr as usize;
        // program counter
        let mut tmp = [0u8; 8];
        {
            let heap = self.heap.read().unwrap();
            heap.read_all(idx..idx + 8, &mut tmp);
            idx += 8;
            self.pc = u64::from_le_bytes(tmp) as usize;
            // stack base
            heap.read_all(idx..idx + 8, &mut tmp);
            idx += 8;
            self.base = u64::from_le_bytes(tmp) as usize;
        }
        // registers
        for i in 0..8 {
            self.heap.read().unwrap().read_all(idx..idx + 8, &mut tmp);
            *self.get_reg(i) = tmp;
            idx += 8;
        }
        self.flg = self.heap.read().unwrap().read_at(idx);
        self.sp = ptr.to_le_bytes();
    }
    fn spawn_process(&mut self) {
        let ptr = u64::from_le_bytes(self.a) as usize;
        let size = u64::from_le_bytes(self.b) as usize;
        let prio = u64::from_le_bytes(self.c) as u32;
        let mut name = String::new();
        for i in 0..size {
            name.push(self.heap.read().unwrap().read_at(ptr + i) as char);
        }
        let path =
            shexp::env(&name).expect("Failed to expand global variables while opening file!");
        match std::fs::read(&*path) {
            Ok(f) => {
                let pid = self.load_prog(f, prio);
                self.a = pid.to_le_bytes();
            }
            Err(_e) => {
                eprintln!("Error: unable to open file {}.", name);
                std::process::exit(1);
            }
        }
    }
    fn file_load(&mut self) {
        let ptr = u64::from_le_bytes(self.a) as usize;
        let size = u64::from_le_bytes(self.b) as usize;
        let mut name = String::new();
        for i in 0..size {
            name.push(self.heap.read().unwrap().read_at(ptr + i) as char);
        }
        let path = match shexp::env(&name) {
            Ok(a) => a,
            Err(e) => {
                eprintln!("Error while expanding environment variables: {e}.");
                self.load_imm(0, 1);
                self.prog_exit();
                return;
            }
        };
        match std::fs::read(&*path) {
            Ok(f) => {
                let ptr = self.heap.write().unwrap().malloc(f.len() as u64);
                self.heap.read().unwrap().write_all(ptr as usize, &f);
                self.a = ptr.to_le_bytes();
            }
            Err(e) => {
                eprintln!("Error: unable to open file {name} with error: {e}.");
                self.load_imm(0, 1);
                self.prog_exit();
            }
        }
    }

    fn load_nice_natives(&mut self) {
        self.push_native(Self::alloc); // 0
        self.push_native(Self::free); // 1
        self.push_native(Self::print_char); // 2
        self.push_native(Self::print_int); // 3
        self.push_native(Self::print_uint); // 4
        self.push_native(Self::print_f32); // 5
        self.push_native(Self::print_f64); // 6
        self.push_native(Self::read); // 7
        self.push_native(Self::debug); // 8
        self.push_native(Self::print_ascii_str); // 9
        self.push_native(Self::prog_exit); // 10
        self.push_native(Self::proc_switch_store); // 11
        self.push_native(Self::proc_switch_restore); // 12
        self.push_native(Self::spawn_process); // 13
        self.push_native(Self::file_load); // 14
    }
}
