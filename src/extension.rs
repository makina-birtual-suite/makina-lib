use crate::{HandleAsm, AllocatorInterface};
//use flate2::read::GzDecoder;
use std::collections::HashMap;
//use tar::Archive;
// Extensions are extra instructions that are defined on the extra 16 bits that are usually empty
// for standard instructions. The first 8 bits define the id of the Extension, and the remaining
// are Extension specific.

pub trait VMXtension: SafeHeapExtension {
    fn execute_extensions(&mut self, dest: u8, src1: u8, src2: u8, imm: u32, inst: u16) {
        match inst >> 8 {
            0 => self.safe_heap_extension_handler(dest, src1, src2, inst as u8, imm),
            _ => unreachable!(),
        }
    }
    fn codegen_ext(
        &self,
        words: Vec<&str>,
        labels: &mut HashMap<String, usize>,
        _constants: &mut HashMap<String, usize>,
        inter_insts: &mut Vec<HandleAsm>,
    ) {
        const EXT_INST: u64 = (15 as u64) << 57;
        const EXT_SAFE_HEAP: u8 = 0;
        let _ = labels;
        match words[0] {
            "lb_s" => {
                const EXT_CODE: u8 = 0;
                let mut final_inst =
                    EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
                if let Some(dest) = words.get(1) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(dest);
                    final_inst += (reg_code as u64) << 54;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting Destination argument!");
                    std::process::exit(1);
                }
                if let Some(src1) = words.get(2) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src1);
                    final_inst += (reg_code as u64) << 51;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
                    std::process::exit(1);
                }
                if let Some(src2) = words.get(3) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src2);
                    final_inst += (reg_code as u64) << 48;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC2 argument!");
                    std::process::exit(1);
                }
                inter_insts.push(HandleAsm::Other(final_inst));
            }
            "lq_s" => {
                const EXT_CODE: u8 = 1;
                let mut final_inst =
                    EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
                if let Some(dest) = words.get(1) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(dest);
                    final_inst += (reg_code as u64) << 54;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting Destination argument!");
                    std::process::exit(1);
                }
                if let Some(src1) = words.get(2) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src1);
                    final_inst += (reg_code as u64) << 51;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
                    std::process::exit(1);
                }
                if let Some(src2) = words.get(3) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src2);
                    final_inst += (reg_code as u64) << 48;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC2 argument!");
                    std::process::exit(1);
                }
                inter_insts.push(HandleAsm::Other(final_inst));
            }
            "lh_s" => {
                const EXT_CODE: u8 = 2;
                let mut final_inst =
                    EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
                if let Some(dest) = words.get(1) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(dest);
                    final_inst += (reg_code as u64) << 54;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting Destination argument!");
                    std::process::exit(1);
                }
                if let Some(src1) = words.get(2) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src1);
                    final_inst += (reg_code as u64) << 51;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
                    std::process::exit(1);
                }
                if let Some(src2) = words.get(3) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src2);
                    final_inst += (reg_code as u64) << 48;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC2 argument!");
                    std::process::exit(1);
                }
                inter_insts.push(HandleAsm::Other(final_inst));
            }
            "lw_s" => {
                const EXT_CODE: u8 = 3;
                let mut final_inst =
                    EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
                if let Some(dest) = words.get(1) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(dest);
                    final_inst += (reg_code as u64) << 54;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting Destination argument!");
                    std::process::exit(1);
                }
                if let Some(src1) = words.get(2) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src1);
                    final_inst += (reg_code as u64) << 51;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
                    std::process::exit(1);
                }
                if let Some(src2) = words.get(3) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src2);
                    final_inst += (reg_code as u64) << 48;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC2 argument!");
                    std::process::exit(1);
                }
                inter_insts.push(HandleAsm::Other(final_inst));
            }
            "sb_s" => {
                const EXT_CODE: u8 = 4;
                let mut final_inst =
                    EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
                if let Some(dest) = words.get(1) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(dest);
                    final_inst += (reg_code as u64) << 54;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting Destination argument!");
                    std::process::exit(1);
                }
                if let Some(src1) = words.get(2) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src1);
                    final_inst += (reg_code as u64) << 51;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
                    std::process::exit(1);
                }
                if let Some(src2) = words.get(3) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src2);
                    final_inst += (reg_code as u64) << 48;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC2 argument!");
                    std::process::exit(1);
                }
                inter_insts.push(HandleAsm::Other(final_inst));
            }
            "sq_s" => {
                const EXT_CODE: u8 = 5;
                let mut final_inst =
                    EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
                if let Some(dest) = words.get(1) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(dest);
                    final_inst += (reg_code as u64) << 54;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting Destination argument!");
                    std::process::exit(1);
                }
                if let Some(src1) = words.get(2) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src1);
                    final_inst += (reg_code as u64) << 51;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
                    std::process::exit(1);
                }
                if let Some(src2) = words.get(3) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src2);
                    final_inst += (reg_code as u64) << 48;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC2 argument!");
                    std::process::exit(1);
                }
                inter_insts.push(HandleAsm::Other(final_inst));
            }
            "sh_s" => {
                const EXT_CODE: u8 = 6;
                let mut final_inst =
                    EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
                if let Some(dest) = words.get(1) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(dest);
                    final_inst += (reg_code as u64) << 54;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting Destination argument!");
                    std::process::exit(1);
                }
                if let Some(src1) = words.get(2) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src1);
                    final_inst += (reg_code as u64) << 51;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
                    std::process::exit(1);
                }
                if let Some(src2) = words.get(3) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src2);
                    final_inst += (reg_code as u64) << 48;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC2 argument!");
                    std::process::exit(1);
                }
                inter_insts.push(HandleAsm::Other(final_inst));
            }
            "sw_s" => {
                const EXT_CODE: u8 = 7;
                let mut final_inst =
                    EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
                if let Some(dest) = words.get(1) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(dest);
                    final_inst += (reg_code as u64) << 54;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting Destination argument!");
                    std::process::exit(1);
                }
                if let Some(src1) = words.get(2) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src1);
                    final_inst += (reg_code as u64) << 51;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
                    std::process::exit(1);
                }
                if let Some(src2) = words.get(3) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(src2);
                    final_inst += (reg_code as u64) << 48;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting SRC2 argument!");
                    std::process::exit(1);
                }
                inter_insts.push(HandleAsm::Other(final_inst));
            }
            "la" => {
                const EXT_CODE: u8 = 8;
                let mut final_inst =
                    EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
                if let Some(dest) = words.get(1) {
                    let reg_code = crate::codegen::get_num_reg_from_asm(dest);
                    final_inst += (reg_code as u64) << 54;
                } else {
                    println!("SafeHeapExtension ERROR: was expecting Destination argument!");
                    std::process::exit(1);
                }
                if let Some(imm) = words.get(2) {
                    inter_insts.push(HandleAsm::UnresolvedCNT(final_inst, (*imm).to_owned()));
                } else {
                    println!("SafeHeapExtension ERROR: was expecting Source argument!");
                    std::process::exit(1);
                }
            }
            //"ref" => {
            //    const EXT_CODE: u8 = 8;
            //    let mut final_inst =
            //        EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
            //    if let Some(dest) = words.get(1) {
            //        let reg_code = crate::codegen::get_num_reg_from_asm(dest);
            //        final_inst += (reg_code as u64) << 54;
            //    } else {
            //        println!("SafeHeapExtension ERROR: was expecting Destination argument!");
            //        std::process::exit(1);
            //    }
            //    if let Some(src1) = words.get(2) {
            //        let reg_code = crate::codegen::get_num_reg_from_asm(src1);
            //        final_inst += (reg_code as u64) << 51;
            //    } else {
            //        println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
            //        std::process::exit(1);
            //    }
            //    if let Some(src2) = words.get(3) {
            //        let reg_code = crate::codegen::get_num_reg_from_asm(src2);
            //        final_inst += (reg_code as u64) << 48;
            //    } else {
            //        println!("SafeHeapExtension ERROR: was expecting SRC2 argument!");
            //        std::process::exit(1);
            //    }
            //    inter_insts.push(HandleAsm::Other(final_inst));
            //}
            //"dlopen" => {
            //    const EXT_CODE: u8 = 9;
            //    let mut final_inst =
            //        EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
            //    if let Some(dest) = words.get(1) {
            //        let reg_code = crate::codegen::get_num_reg_from_asm(dest);
            //        final_inst += (reg_code as u64) << 54;
            //    } else {
            //        println!("SafeHeapExtension ERROR: was expecting Destination argument!");
            //        std::process::exit(1);
            //    }
            //    if let Some(src1) = words.get(2) {
            //        let reg_code = crate::codegen::get_num_reg_from_asm(src1);
            //        final_inst += (reg_code as u64) << 51;
            //    } else {
            //        println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
            //        std::process::exit(1);
            //    }
            //    inter_insts.push(HandleAsm::Other(final_inst));
            //}
            //"dlsym" => {
            //    const EXT_CODE: u8 = 10;
            //    let mut final_inst =
            //        EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
            //    if let Some(dest) = words.get(2) {
            //        let reg_code = crate::codegen::get_num_reg_from_asm(dest);
            //        final_inst += (reg_code as u64) << 54;
            //    } else {
            //        println!("SafeHeapExtension ERROR: was expecting Destination argument!");
            //        std::process::exit(1);
            //    }
            //    if let Some(src1) = words.get(3) {
            //        let reg_code = crate::codegen::get_num_reg_from_asm(src1);
            //        final_inst += (reg_code as u64) << 51;
            //    } else {
            //        println!("SafeHeapExtension ERROR: was expecting SRC1 argument!");
            //        std::process::exit(1);
            //    }
            //    if let Some(src2) = words.get(4) {
            //        let reg_code = crate::codegen::get_num_reg_from_asm(src2);
            //        final_inst += (reg_code as u64) << 48;
            //    } else {
            //        println!("SafeHeapExtension ERROR: was expecting SRC2 argument!");
            //        std::process::exit(1);
            //    }
            //    if let Some(imm) = words.get(1) {
            //        if let Some(immval) = constants.get(imm.to_owned()) {
            //            final_inst += *immval as u32 as u64;
            //        } else {
            //            inter_insts.push(HandleAsm::UnresolvedCNT(final_inst, (*imm).to_owned()));
            //        }
            //    } else {
            //        println!("SafeHeapExtension ERROR: was expecting Destination argument!");
            //        std::process::exit(1);
            //    }
            //    inter_insts.push(HandleAsm::Other(final_inst));
            //}
            //"callr" => {
            //    const EXT_CODE: u8 = 11;
            //    let mut final_inst =
            //        EXT_INST + ((EXT_SAFE_HEAP as u64) << 40) + ((EXT_CODE as u64) << 32);
            //    if let Some(imm) = words.get(1) {
            //        if let Some(immval) = constants.get(imm.to_owned()) {
            //            final_inst += *immval as u32 as u64;
            //        } else {
            //            inter_insts.push(HandleAsm::UnresolvedCNT(final_inst, (*imm).to_owned()));
            //        }
            //    } else {
            //        println!("SafeHeapExtension ERROR: was expecting Destination argument!");
            //        std::process::exit(1);
            //    }
            //    inter_insts.push(HandleAsm::Other(final_inst));
            //}
            a => panic!("Not implemented: {}", a),
        }
    }
}

/// Extension 00000000
/// This provides safe alternative instructions that auto-deref of the smart pointer into the rawptr i
/// nternally, without exposing the
/// rawptr to the registers, allowing safe access to the memory. The right hand source contains the
/// offset, in bytes.
pub trait SafeHeapExtension {
    /// Instruction 00000000 -- lb_s   REG REG REG (destreg <- VirtPtr, (offset : u8))
    fn safe_load_8(&mut self, dest: u8, src1: u8, src2: u8);
    /// Instruction 00000001 -- lq_s
    fn safe_load_16(&mut self, dest: u8, src1: u8, src2: u8);
    /// Instruction 00000010 -- lh_s
    fn safe_load_32(&mut self, dest: u8, src1: u8, src2: u8);
    /// Instruction 00000011 -- lw_s
    fn safe_load_64(&mut self, dest: u8, src1: u8, src2: u8);
    /// Instruction 00000100 -- sb_s   REG REG REG (VirtPtr <- srcreg, (offset : u8))
    fn safe_store_8(&mut self, dest: u8, src1: u8, src2: u8);
    /// Instruction 00000101 -- sq_s
    fn safe_store_16(&mut self, dest: u8, src1: u8, src2: u8);
    /// Instruction 00000110 -- sh_s
    fn safe_store_32(&mut self, dest: u8, src1: u8, src2: u8);
    /// Instruction 00000111 -- sw_s
    fn safe_store_64(&mut self, dest: u8, src1: u8, src2: u8);
    /// Instruction 00001000 -- la  reg imm 
    fn load_address(&mut self, dest : u8, imm : u32);
    //fn refer_to_data(&mut self, dest: u8, src1: u8, src2: u8);
    ///// Instruction 00001001 -- dlopen reg reg (dst <- MAP*, src1 <- code*) (you give the ptr to
    ///// the string in the first argument, the second register can be empty)
    //fn dlopen(&mut self, dest: u8, src1: u8);
    ///// Instruction 00001010 -- dlsym IMM reg reg (dst is imm, pointer to a variable of size 8 +
    ///// size of sym, dst contains MAP*, src1 contains code* and src2 a pointer to a string with the name of the symbol)
    //fn dlsym(&mut self, dest: u8, src1: u8, src2: u8, imm: u32);
    ///// Instruction 00001011 -- callr IMM, Uses a pointer that contains 16 bytes of data, first is the ptr to the code and the second byte is the offset
    //fn callr(&mut self, imm: u32);

    /// Handler for SafeHeapExtension
    fn safe_heap_extension_handler(&mut self, dest: u8, src1: u8, src2: u8, code: u8, imm: u32) {
        match code {
            0 => self.safe_load_8(dest, src1, src2),
            1 => self.safe_load_16(dest, src1, src2),
            2 => self.safe_load_32(dest, src1, src2),
            3 => self.safe_load_64(dest, src1, src2),
            4 => self.safe_store_8(dest, src1, src2),
            5 => self.safe_store_16(dest, src1, src2),
            6 => self.safe_store_32(dest, src1, src2),
            7 => self.safe_store_64(dest, src1, src2),
            8 => self.load_address(dest, imm),
            //8 => self.refer_to_data(dest, src1, src2),
            //9 => self.dlopen(dest, src1),
            //10 => self.dlsym(dest, src1, src2, imm),
            //11 => self.callr(imm),
            8..=11 => panic!("Dynamic library support has been removed."), 
            _ => unreachable!(),
        }
    }
}
impl<T : AllocatorInterface> SafeHeapExtension for crate::VM<T> {
    fn safe_load_8(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            dest < 8 && src1 < 8 && src2 < 8,
            "Registers are 0..7, out of range"
        );
        todo!()
        // let ptr = u64::from_le_bytes(*self.get_reg(src1));
        // let offset = u64::from_le_bytes(*self.get_reg(src2));
        // self.get_reg(dest)[0] = self.heap.read_at(ptr as usize + offset as usize);
    }
    fn safe_load_16(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            dest < 8 && src1 < 8 && src2 < 8,
            "Registers are 0..7, out of range"
        );
        todo!()
        // let ptr = u64::from_le_bytes(*self.get_reg(src1));
        // let offset = u64::from_le_bytes(*self.get_reg(src2));
        // self.get_reg(dest)[0] = self.heap.read_at((ptr+offset) as usize);
        // self.get_reg(dest)[1] = self.heap.read_at((ptr + offset + 1) as usize);
    }
    fn safe_load_32(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            dest < 8 && src1 < 8 && src2 < 8,
            "Registers are 0..7, out of range"
        );
        let _ptr = u64::from_le_bytes(*self.get_reg(src1));
        let _offset = u64::from_le_bytes(*self.get_reg(src2));
        todo!();
        // self.get_reg(dest)[0] = self.heap[(ptr + offset) as usize];
        // self.get_reg(dest)[1] = self.heap[(ptr + offset + 1) as usize];
        // self.get_reg(dest)[2] = self.heap[(ptr + offset + 2) as usize];
        // self.get_reg(dest)[3] = self.heap[(ptr + offset + 3) as usize];
    }
    fn safe_load_64(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            dest < 8 && src1 < 8 && src2 < 8,
            "Registers are 0..7, out of range"
        );
        let _ptr = u64::from_le_bytes(*self.get_reg(src1));
        let _offset = u64::from_le_bytes(*self.get_reg(src2));
        todo!()
        // self.get_reg(dest)[0] = self.heap[(ptr + offset) as usize];
        // self.get_reg(dest)[1] = self.heap[(ptr + offset + 1) as usize];
        // self.get_reg(dest)[2] = self.heap[(ptr + offset + 2) as usize];
        // self.get_reg(dest)[3] = self.heap[(ptr + offset + 3) as usize];
        // self.get_reg(dest)[4] = self.heap[(ptr + offset + 4) as usize];
        // self.get_reg(dest)[5] = self.heap[(ptr + offset + 5) as usize];
        // self.get_reg(dest)[6] = self.heap[(ptr + offset + 6) as usize];
        // self.get_reg(dest)[7] = self.heap[(ptr + offset + 7) as usize];
}
    fn safe_store_8(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            dest < 8 && src1 < 8 && src2 < 8,
            "Registers are 0..7, out of range"
        );
        let _ptr = u64::from_le_bytes(*self.get_reg(dest));
        let _offset = u64::from_le_bytes(*self.get_reg(src2));
        // self.heap[(ptr + offset) as usize] = self.get_reg(src1)[0];
        todo!()
    }
    fn safe_store_16(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            dest < 8 && src1 < 8 && src2 < 8,
            "Registers are 0..7, out of range"
        );
        let _ptr = u64::from_le_bytes(*self.get_reg(dest));
        let _offset = u64::from_le_bytes(*self.get_reg(src2));
        todo!()
        // self.heap[(ptr + offset) as usize] = self.get_reg(src1)[0];
        // self.heap[(ptr + offset + 1) as usize] = self.get_reg(src1)[1];
    }
    fn safe_store_32(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            dest < 8 && src1 < 8 && src2 < 8,
            "Registers are 0..7, out of range"
        );
        let _ptr = u64::from_le_bytes(*self.get_reg(dest));
        let _offset = u64::from_le_bytes(*self.get_reg(src2));
        todo!()
        // self.heap[(ptr + offset) as usize] = self.get_reg(src1)[0];
        // self.heap[(ptr + offset + 1) as usize] = self.get_reg(src1)[1];
        // self.heap[(ptr + offset + 2) as usize] = self.get_reg(src1)[2];
        // self.heap[(ptr + offset + 3) as usize] = self.get_reg(src1)[3];
    }
    fn safe_store_64(&mut self, dest: u8, src1: u8, src2: u8) {
        assert!(
            dest < 8 && src1 < 8 && src2 < 8,
            "Registers are 0..7, out of range"
        );
        let _ptr = u64::from_le_bytes(*self.get_reg(dest));
        let _offset = u64::from_le_bytes(*self.get_reg(src2));
        todo!()
        // self.heap[(ptr + offset) as usize] = self.get_reg(src1)[0];
        // self.heap[(ptr + offset + 1) as usize] = self.get_reg(src1)[1];
        // self.heap[(ptr + offset + 2) as usize] = self.get_reg(src1)[2];
        // self.heap[(ptr + offset + 3) as usize] = self.get_reg(src1)[3];
        // self.heap[(ptr + offset + 4) as usize] = self.get_reg(src1)[4];
        // self.heap[(ptr + offset + 5) as usize] = self.get_reg(src1)[5];
        // self.heap[(ptr + offset + 6) as usize] = self.get_reg(src1)[6];
        // self.heap[(ptr + offset + 7) as usize] = self.get_reg(src1)[7];
    }
    fn load_address(&mut self, dest : u8, imm : u32) {
        assert!(dest < 8, "Registers are 0..7, out of range");
        let res = (self.heap.read().unwrap().parentalloc(self.pc as u64) + imm as i32 as i64 as u64).to_le_bytes();
        *self.get_reg(dest) = res;
    }
    //fn refer_to_data(&mut self, dest: u8, src1: u8, src2: u8) {
    //    assert!(
    //        dest < 8 && src1 < 8 && src2 < 8,
    //        "Registers are 0..7, out of range"
    //    );
    //    let offset = u64::from_le_bytes(*self.get_reg(dest));
    //    let size = u64::from_le_bytes(*self.get_reg(src1));
    //    let orig_ptr = u64::from_le_bytes(*self.get_reg(src2));
    //    *self.get_reg(dest) = self.heap.mrefer_to(orig_ptr, offset, size).to_le_bytes();
    //}
    //fn dlopen(&mut self, dest: u8, src1: u8) {
    //    let libname = u64::from_le_bytes(*self.get_reg(dest));
    //    if let (Some(libname_pos), Some(libname_size)) = (
    //        self.heap.get_raw_ptr(libname),
    //        self.heap.size_of_alloc(libname),
    //    ) {
    //        let libname = String::from_utf8(
    //            self.heap.fst_arena.memory
    //                [libname_pos as usize..libname_pos as usize + libname_size as usize]
    //                .to_vec(),
    //        )
    //        .expect("unreachable dlopen");
    //        let tar_gz = std::fs::File::open(libname)
    //            .expect("Provided path for dyn lib is invalid (file does not exist)");
    //        let tar = GzDecoder::new(tar_gz);
    //        let mut archive = Archive::new(tar);
    //        let mut map = String::new();
    //        let mut code = Vec::new();
    //        let data = archive.entries().expect("file is not a valid library");
    //        for datum in data {
    //            if let Ok(mut entry) = datum {
    //                //println!("{:?}", entry.path());
    //                match entry.path() {
    //                    Ok(a) if a.to_str() == Some("MAP") => {
    //                        entry.read_to_string(&mut map).expect("could not read MAP")
    //                    }
    //                    Ok(a) if a.to_str() == Some("lib.mknc") => {
    //                        entry.read_to_end(&mut code).expect("could not read code")
    //                    }
    //                    Ok(_) => continue,
    //                    Err(e) => panic!("ERROR: {e}"),
    //                };
    //            };
    //        }
    //        let map_bytes = map.as_bytes();
    //        let map_size = map_bytes.len();
    //        let map_ptr = self.heap.malloc(map_size as u64);
    //        let map_raw = self.heap.get_raw_ptr(map_ptr).unwrap();
    //        for i in 0..map_size {
    //            self.heap.fst_arena.memory[map_raw as usize + i] = map_bytes[i];
    //        }
    //        *self.get_reg(dest) = map_ptr.to_le_bytes();
    //        let code_bytes = &code[..];
    //        let code_size = code_bytes.len();
    //        let code_ptr = self.heap.malloc(code_size as u64);
    //        let code_raw = self.heap.get_raw_ptr(code_ptr).unwrap();
    //        for i in 0..code_size {
    //            self.heap.fst_arena.memory[code_raw as usize + i] = code_bytes[i];
    //        }
    //        *self.get_reg(src1) = code_ptr.to_le_bytes();
    //    }
    //}
    //fn dlsym(&mut self, dest: u8, src1: u8, src2: u8, imm: u32) {
    //    const MSG: &'static str = "Failed to convert to utf8";
    //    let map = u64::from_le_bytes(*self.get_reg(dest));
    //    if let (Some(map_pos), Some(map_size)) =
    //        (self.heap.get_raw_ptr(map), self.heap.size_of_alloc(map))
    //    {
    //        let map = String::from_utf8(
    //            self.heap.fst_arena.memory[map_pos as usize..map_pos as usize + map_size as usize]
    //                .to_vec(),
    //        )
    //        .expect(MSG);
    //        let sym = u64::from_le_bytes(*self.get_reg(src2));
    //        if let (Some(sym_pos), Some(sym_size)) =
    //            (self.heap.get_raw_ptr(sym), self.heap.size_of_alloc(sym))
    //        {
    //            let sym = String::from_utf8(
    //                self.heap.fst_arena.memory
    //                    [sym_pos as usize..sym_pos as usize + sym_size as usize]
    //                    .to_vec(),
    //            )
    //            .expect(MSG);
    //            for line in map.lines() {
    //                let map_pair = line.split_whitespace().collect::<Vec<&str>>();
    //                if map_pair[0] == sym {
    //                    if let Some(rawptr) = self.heap.get_raw_ptr(imm as u64) {
    //                        let value = map_pair[1]
    //                            .parse::<u64>()
    //                            .expect("value is not a ptr")
    //                            .to_le_bytes();
    //                        for i in 0..16 {
    //                            if i < 8 {
    //                                self.heap.fst_arena.memory[rawptr as usize + i] =
    //                                    self.get_reg(src1)[i];
    //                            } else {
    //                                self.heap.fst_arena.memory[rawptr as usize + i] = value[i - 8];
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}
    //fn callr(&mut self, imm: u32) {
    //    if let Some(ptr) = self.heap.get_raw_ptr(imm as u64) {
    //        let mut code_ptr: [u8; 8] = [0; 8];
    //        code_ptr[0] = self.heap[ptr as usize];
    //        code_ptr[1] = self.heap[ptr as usize + 1];
    //        code_ptr[2] = self.heap[ptr as usize + 2];
    //        code_ptr[3] = self.heap[ptr as usize + 3];
    //        code_ptr[4] = self.heap[ptr as usize + 4];
    //        code_ptr[5] = self.heap[ptr as usize + 5];
    //        code_ptr[6] = self.heap[ptr as usize + 6];
    //        code_ptr[7] = self.heap[ptr as usize + 7];
    //        let code_ptr = u64::from_le_bytes(code_ptr);
    //        let mut offset: [u8; 8] = [0; 8];
    //        offset[0] = self.heap[ptr as usize + 8];
    //        offset[1] = self.heap[ptr as usize + 9];
    //        offset[2] = self.heap[ptr as usize + 10];
    //        offset[3] = self.heap[ptr as usize + 11];
    //        offset[4] = self.heap[ptr as usize + 12];
    //        offset[5] = self.heap[ptr as usize + 13];
    //        offset[6] = self.heap[ptr as usize + 14];
    //        offset[7] = self.heap[ptr as usize + 15];
    //        let offset = u64::from_le_bytes(offset);
    //        if let Some(base) = self.heap.get_raw_ptr(code_ptr) {
    //            self.stack.push(self.pc as u64);
    //            self.pc = (base + offset) as usize;
    //        }
    //    }
    //}
}
