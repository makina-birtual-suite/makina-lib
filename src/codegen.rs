use crate::HandleAsm;
use crate::VMXtension;
use crate::INST_HALT_VM;
use ::shellexpand as shexp;
use core::panic;
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;
pub const MKNBDATA : [u8;8] = [77,75,78,66,68,65,84,65];
pub const MKNBEXEC : [u8;8] = [77,75,78,66,69,88,69,67];

pub fn read_asm_file(file: &str) -> String {
    let f = File::open(file);
    match f {
        Ok(s) => {
            let mut buf = String::new();
            match BufReader::new(s).read_to_string(&mut buf) {
                Ok(_) => buf,
                _ => panic!("could not read file {file}"),
            }
        }
        Err(e) => panic!("{e} {file}"),
    }
}

pub fn get_num_reg_from_asm(s: &str) -> u8 {
    return match s {
        "a" => 0,
        "b" => 1,
        "c" => 2,
        "d" => 3,
        "e" => 4,
        "f" => 5,
        "g" => 6,
        "sp" => 7,
        _ => {
            eprintln!("Error: unknown register {}.", s);
            std::process::exit(1);
        }
    };
}

pub trait Codegen: VMXtension {
    fn asm_to_bytecode(
        &self,
        s: String,
        included: &mut Vec<String>,
    ) -> (Vec<u8>, Vec<(String, usize)>) {
        const AVAIL_REGS: [&'static str; 8] = ["a", "b", "c", "d", "e", "f", "g", "sp"];
        let mut labels: HashMap<String, usize> = HashMap::new();
        let mut constants: HashMap<String, usize> = HashMap::new();
        let mut inter_insts: Vec<HandleAsm> = Vec::new();
        let mut ptr_base = 1u32;
        let mut ret = Vec::new();
        inter_insts.push(HandleAsm::Other(u64::from_ne_bytes(MKNBEXEC)));
        for inst in s.lines() {
            let words: Vec<&str> = inst.split_whitespace().collect();
            if words.len() < 1 {
                continue;
            }
            match words[0] {
                "include" => {
                    // Includes another .mknasm file, following the **Library Convention**
                    // The library convention is basically a label at the end of the library and a jump
                    // at the beggining of the library to that label, in order to not run code.
                    // Note that this isn't enforced, so libaries can execute arbritary code.
                    if included.contains(&words[1].to_owned()) {
                        continue;
                    } else {
                        included.push(words[1].to_owned());
                        let path = shexp::env(words[1])
                            .expect("Failed to expand global variables while opening file!");
                        let asm = read_asm_file(&path);
                        let mut new_s = String::new();
                        for inst in s.lines() {
                            if inst.split_whitespace().collect::<Vec<&str>>() != words {
                                new_s += inst;
                                new_s += "\n";
                            }
                        }
                        new_s += &asm;
                        //println!("{}", &new_s);
                        return Self::asm_to_bytecode(self, new_s, included);
                    }
                }
                "byte" => {
                    if let Ok(val) = words[2].parse::<u8>() {
                        constants.insert(words[1].to_owned(), val as usize);
                    } else {
                        eprintln!("{} is not byte-sized!", words[2]);
                        std::process::exit(1);
                    }
                }
                "byte*" => {
                    let size_alloc = words.len() - 2;
                    let name = words[1].to_owned();
                    constants.insert(format!("{name}::size"), size_alloc);
                    constants.insert(name, ptr_base as usize);
                    let mut bites = Vec::new();
                    for i in 2..words.len() {
                        if let Ok(bite) = words[i].parse::<u8>() {
                            bites.push(bite);
                        } else {
                            eprintln!("{} is not byte-sized!", words[1]);
                            std::process::exit(1);
                        }
                    }
                    inter_insts.push(HandleAsm::UnresolvedRef(bites));
                    ptr_base += 1;
                }
                "quarter" => {
                    if let Ok(val) = words[2].parse::<u16>() {
                        constants.insert(words[1].to_owned(), val as usize);
                    } else {
                        eprintln!("{} is not quarter-sized!", words[2]);
                        std::process::exit(1);
                    }
                }
                "quarter*" => {
                    let size_alloc = (words.len() - 2) * 2;
                    let name = words[1].to_owned();
                    constants.insert(format!("{name}::size"), size_alloc);
                    constants.insert(name, ptr_base as usize);
                    let mut bites = Vec::new();
                    for i in 2..words.len() {
                        if let Ok(bite) = words[i].parse::<u16>().map(|x| x.to_le_bytes()) {
                            bites.push(bite[0]);
                            bites.push(bite[1]);
                        } else {
                            eprintln!("{} is not byte-sized!", words[1]);
                            std::process::exit(1);
                        }
                    }
                    ptr_base += 1;
                    inter_insts.push(HandleAsm::UnresolvedRef(bites)); // li b <offset>
                }
                "half_u" => {
                    if let Ok(val) = words[2].parse::<u32>() {
                        constants.insert(words[1].to_owned(), val as usize);
                    } else {
                        eprintln!("{} is not half-sized!", words[2]);
                        std::process::exit(1);
                    }
                }
                "half_u*" => {
                    let size_alloc = (words.len() - 2) * 4;
                    let name = words[1].to_owned();
                    constants.insert(format!("{name}::size"), size_alloc);
                    constants.insert(name, ptr_base as usize);
                    let mut bites = Vec::new();
                    for i in 2..words.len() {
                        if let Ok(bite) = words[i].parse::<u32>().map(|x| x.to_le_bytes()) {
                            bites.push(bite[0]);
                            bites.push(bite[1]);
                            bites.push(bite[2]);
                            bites.push(bite[3]);
                        } else {
                            eprintln!("{} is not byte-sized!", words[1]);
                            std::process::exit(1);
                        }
                    }
                    inter_insts.push(HandleAsm::UnresolvedRef(bites)); // li b <offset>
                    ptr_base += 1; 
                }
                "half_i" => {
                    if let Ok(val) = words[2].parse::<i32>() {
                        constants.insert(words[1].to_owned(), val as usize);
                    } else {
                        eprintln!("{} is not half-sized!", words[2]);
                        std::process::exit(1);
                    }
                }
                "half_i*" => {
                    let size_alloc = (words.len() - 2) * 4;
                    let name = words[1].to_owned();
                    constants.insert(format!("{name}::size"), size_alloc);
                    constants.insert(name, ptr_base as usize);
                    let mut bites = Vec::new();
                    for i in 2..words.len() {
                        if let Ok(bite) = words[i].parse::<i32>().map(|x| x.to_le_bytes()) {
                            bites.push(bite[0]);
                            bites.push(bite[1]);
                            bites.push(bite[2]);
                            bites.push(bite[3]);
                        } else {
                            eprintln!("{} is not byte-sized!", words[1]);
                            std::process::exit(1);
                        }
                    }
                    inter_insts.push(HandleAsm::UnresolvedRef(bites)); // li b <offset>
                    ptr_base += 1; 
                }
                "word_u" => {
                    if let Ok(val) = words[2].parse::<u64>() {
                        constants.insert(words[1].to_owned(), val as usize);
                    } else {
                        eprintln!("{} is not word-sized!", words[2]);
                        std::process::exit(1);
                    }
                }
                "word_u*" => {
                    let size_alloc = (words.len() - 2) * 8;
                    let name = words[1].to_owned();
                    constants.insert(format!("{name}::size"), size_alloc);
                    constants.insert(name, ptr_base as usize);
                    let mut bites = Vec::new();
                    for i in 2..words.len() {
                        if let Ok(bite) = words[i].parse::<u64>().map(|x| x.to_le_bytes()) {
                            bites.push(bite[0]);
                            bites.push(bite[1]);
                            bites.push(bite[2]);
                            bites.push(bite[3]);
                            bites.push(bite[4]);
                            bites.push(bite[5]);
                            bites.push(bite[6]);
                            bites.push(bite[7]);
                        } else {
                            eprintln!("{} is not byte-sized!", words[1]);
                            std::process::exit(1);
                        }
                    }
                    inter_insts.push(HandleAsm::UnresolvedRef(bites)); // li b <offset>
                    ptr_base += 1;
                }
                "word_i" => {
                    if let Ok(val) = words[2].parse::<i64>() {
                        constants.insert(words[1].to_owned(), val as usize);
                    } else {
                        eprintln!("{} is not word-sized!", words[2]);
                        std::process::exit(1);
                    }
                }
                "word_i*" => {
                    let size_alloc = (words.len() - 2) * 8;
                    let name = words[1].to_owned();
                    constants.insert(format!("{name}::size"), size_alloc);
                    constants.insert(name, ptr_base as usize);
                    let mut bites = Vec::new();
                    for i in 2..words.len() {
                        if let Ok(bite) = words[i].parse::<i64>().map(|x| x.to_le_bytes()) {
                            bites.push(bite[0]);
                            bites.push(bite[1]);
                            bites.push(bite[2]);
                            bites.push(bite[3]);
                            bites.push(bite[4]);
                            bites.push(bite[5]);
                            bites.push(bite[6]);
                            bites.push(bite[7]);
                        } else {
                            eprintln!("{} is not byte-sized!", words[1]);
                            std::process::exit(1);
                        }
                    }
                    inter_insts.push(HandleAsm::UnresolvedRef(bites)); // li b <offset>
                    ptr_base += 1;
                }
                "asciistr" => {
                    let mut flag = false;
                    let mut vec_char = Vec::new();
                    for ch in inst.trim().chars() {
                        if flag {
                            match ch {
                                'n' => {
                                    if let Some(prev) = vec_char.pop() {
                                        if prev as char == '\\' {
                                            vec_char.push('\n' as u8);
                                        } else {
                                            vec_char.push(prev);
                                            vec_char.push('n' as u8);
                                        }
                                    } else {
                                        vec_char.push(ch as u8);
                                    }
                                }
                                'e' => {
                                    if let Some(prev) = vec_char.pop() {
                                        if prev as char == '\\' {
                                            vec_char.push(27 as u8);
                                        } else {
                                            vec_char.push(prev);
                                            vec_char.push('e' as u8);
                                        }
                                    } else {
                                        vec_char.push(ch as u8);
                                    }
                                }
                                _ => vec_char.push(ch as u8),
                            }
                        }
                        if ch == '\"' {
                            flag = true;
                        }
                    }
                    vec_char.pop();
                    let size_alloc = vec_char.len();
                    let name = words[1].to_owned();
                    constants.insert(format!("{name}::size"), size_alloc);
                    constants.insert(name, ptr_base as usize);
                    inter_insts.push(HandleAsm::UnresolvedRef(vec_char)); // li b <>
                    ptr_base += 1; 
                }
                // "str" => {
                //     let mut flag = false;
                //     let mut vec_char = Vec::new();
                //     for ch in inst.trim().chars() {
                //         if flag {
                //             vec_char.push(ch as u32);
                //         }
                //         if ch == '\"' {
                //             flag = true;
                //         }
                //     }
                //     vec_char.pop();
                //     let size_alloc = vec_char.len();
                //     let name = words[1].to_owned();
                //     constants.insert(name, ptr_base as usize);
                //     ptr_base += 1;
                //     inter_insts.push(HandleAsm::Other(size_alloc as u64)); // li a <size>
                //     inter_insts.push(HandleAsm::Other(126100789566373888)); // li h 0
                //     inter_insts.push(HandleAsm::Other(1873497444986126336)); // ncall
                //     inter_insts.push(HandleAsm::Other(2485986994308513792)); // addi c a 0
                //     for val in vec_char.iter() {
                //             inter_insts.push(HandleAsm::Other(18014398509481984 + (*val as u64 & 4_294_967_295))); // li b <val>
                //             inter_insts.push(HandleAsm::Other(1047086913363640320)); // sh c b
                //             inter_insts.push(HandleAsm::Other(2490490593935884292)); // addi c c 4
                //     }
                // }
                "label" => {
                    labels.insert(words[1].to_owned(), inter_insts.len() - 1);
                }
                "comm" | ";;" | "//" => {}
                "li" => {
                    let dest = get_num_reg_from_asm(words[1]);
                    let immv = words[2].parse::<i32>();
                    match immv {
                        Ok(v) => {
                            let inst = ((dest as u64) << 54) + (v as u64 & 4_294_967_295);
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                        Err(_) => {
                            if let Some(_) = labels.get(words[2]) {
                                let inst = (dest as u64) << 54;
                                inter_insts
                                    .push(HandleAsm::UnresolvedJMP(inst, words[2].to_owned()))
                            } else if let Some(_) = constants.get(words[2]) {
                                let inst = (dest as u64) << 54;
                                inter_insts
                                    .push(HandleAsm::UnresolvedCNT(inst, words[2].to_owned()))
                            } else {
                                let inst = (dest as u64) << 54;
                                inter_insts
                                    .push(HandleAsm::UnresolvedCNT(inst, words[2].to_owned()))
                            }
                        }
                    }
                }
                "lui" => {
                    let instrinsic_mask = (14 as u64) << 57;
                    let dest = get_num_reg_from_asm(words[1]);
                    let immv = words[2].parse::<i32>();
                    match immv {
                        Ok(v) => {
                            let inst = instrinsic_mask
                                + ((dest as u64) << 54)
                                + (v as u64 & 4_294_967_295);
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                        Err(_) => {
                            if let Some(_addr) = labels.get(words[2]) {
                                let inst = ((dest as u64) << 54) + instrinsic_mask;
                                inter_insts
                                    .push(HandleAsm::UnresolvedJMP(inst, words[2].to_owned()))
                            } else if let Some(_addr) = constants.get(words[2]) {
                                let inst = ((dest as u64) << 54) + instrinsic_mask;
                                inter_insts
                                    .push(HandleAsm::UnresolvedCNT(inst, words[2].to_owned()))
                            } else {
                                let inst = ((dest as u64) << 54) + instrinsic_mask;
                                inter_insts
                                    .push(HandleAsm::UnresolvedCNT(inst, words[2].to_owned()))
                            }
                        }
                    }
                }
                "lb" => {
                    let instrinsic_mask = (1 as u64) << 57;
                    let dest = get_num_reg_from_asm(words[1]);
                    let src = words[2].parse::<u32>();
                    match src {
                        Ok(_v) => {
                            unreachable!();
                            //if words.len() == 4 {
                            //    let offset = words[3]
                            //        .parse::<u32>()
                            //        .expect(&format!("{} is not a valid offset!", words[3]));
                            //    let inst = instrinsic_mask
                            //        + ((dest as u64) << 54)
                            //        + v as u64
                            //        + offset as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //} else {
                            //    let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //}
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&words[2]) {
                                if words.len() == 4 {
                                    let offset = words[3]
                                        .parse::<u32>()
                                        .expect(&format!("{} is not a valid offset!", words[3]));
                                    let inst =
                                        instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                    inter_insts.push(HandleAsm::Other(inst));
                                } else {
                                    let inst = instrinsic_mask
                                        + ((dest as u64) << 54)
                                        + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                    inter_insts.push(HandleAsm::Other(inst))
                                }
                            } else {
                                eprintln!("{} is not a valid addr!", words[2]);
                                std::process::exit(1);
                            }
                        }
                    }
                }
                //"raw" => {
                //    let instrinsic_mask = (5 << 61) + (3 << 58);
                //    let dest = get_num_reg_from_asm(words[1]);
                //    let src = words[2].parse::<u32>();
                //    match src {
                //        Ok(v) => {
                //            if words.len() == 4 {
                //                let offset = words[3]
                //                    .parse::<u32>()
                //                    .expect(&format!("{} is not a valid offset!", words[3]));
                //                let inst = instrinsic_mask
                //                    + ((dest as u64) << 54)
                //                    + v as u64
                //                    + offset as u64;
                //                inter_insts.push(HandleAsm::Other(inst));
                //            } else {
                //                let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                //                inter_insts.push(HandleAsm::Other(inst));
                //            }
                //        }
                //        Err(_) => {
                //            if AVAIL_REGS.contains(&words[2]) {
                //                if words.len() == 4 {
                //                    let offset = words[3]
                //                        .parse::<u32>()
                //                        .expect(&format!("{} is not a valid offset!", words[3]));
                //                    let inst =
                //                        instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                //                    inter_insts.push(HandleAsm::Other(inst));
                //                } else {
                //                    let inst = instrinsic_mask
                //                        + ((dest as u64) << 54)
                //                        + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                //                    inter_insts.push(HandleAsm::Other(inst))
                //                }
                //            } else {
                //                eprintln!("{} is not a valid addr!", words[2]);
                //                std::process::exit(1);
                //            }
                //        }
                //    }
                //}
                "lq" => {
                    let instrinsic_mask = (2 as u64) << 57;
                    let dest = get_num_reg_from_asm(words[1]);
                    let src = words[2].parse::<u32>();
                    match src {
                        Ok(_v) => {
                            unreachable!()
                            //if words.len() == 4 {
                            //    let offset = words[3]
                            //        .parse::<u32>()
                            //        .expect(&format!("{} is not a valid offset!", words[3]));
                            //    let inst = instrinsic_mask
                            //        + ((dest as u64) << 54)
                            //        + v as u64
                            //        + offset as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //} else {
                            //    let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //}
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&words[2]) {
                                if words.len() == 4 {
                                    let offset = words[3]
                                        .parse::<u32>()
                                        .expect(&format!("{} is not a valid offset!", words[3]));
                                    let inst =
                                        instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                    inter_insts.push(HandleAsm::Other(inst));
                                } else {
                                    let inst = instrinsic_mask
                                        + ((dest as u64) << 54)
                                        + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                    inter_insts.push(HandleAsm::Other(inst))
                                }
                            } else {
                                eprintln!("{} is not a valid addr!", words[2]);
                                std::process::exit(1);
                            }
                        }
                    }
                }
                "lh" => {
                    let instrinsic_mask = (3 as u64) << 57;
                    let dest = get_num_reg_from_asm(words[1]);
                    let src = words[2].parse::<u32>();
                    match src {
                        Ok(_v) => {
                            unreachable!()
                            //if words.len() == 4 {
                            //    let offset = words[3]
                            //        .parse::<u32>()
                            //        .expect(&format!("{} is not a valid offset!", words[3]));
                            //    let inst = instrinsic_mask
                            //        + ((dest as u64) << 54)
                            //        + v as u64
                            //        + offset as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //} else {
                            //    let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //}
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&words[2]) {
                                if words.len() == 4 {
                                    let offset = words[3]
                                        .parse::<u32>()
                                        .expect(&format!("{} is not a valid offset!", words[3]));
                                    let inst =
                                        instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                    inter_insts.push(HandleAsm::Other(inst));
                                } else {
                                    let inst = instrinsic_mask
                                        + ((dest as u64) << 54)
                                        + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                    inter_insts.push(HandleAsm::Other(inst))
                                }
                            } else {
                                eprintln!("{} is not a valid addr!", words[2]);
                                std::process::exit(1);
                            }
                        }
                    }
                }
                "lw" => {
                    let instrinsic_mask = (4 as u64) << 57;
                    let dest = get_num_reg_from_asm(words[1]);
                    let src = words[2].parse::<u32>();
                    match src {
                        Ok(_v) => {
                            unreachable!()
                            //if words.len() == 4 {
                            //    let offset = words[3]
                            //        .parse::<u32>()
                            //        .expect(&format!("{} is not a valid offset!", words[3]));
                            //    let inst = instrinsic_mask
                            //        + ((dest as u64) << 54)
                            //        + v as u64
                            //        + offset as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //} else {
                            //    let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //}
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&words[2]) {
                                if words.len() == 4 {
                                    let offset = words[3]
                                        .parse::<u32>()
                                        .expect(&format!("{} is not a valid offset!", words[3]));
                                    let inst =
                                        instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                    inter_insts.push(HandleAsm::Other(inst));
                                } else {
                                    let inst = instrinsic_mask
                                        + ((dest as u64) << 54)
                                        + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                    inter_insts.push(HandleAsm::Other(inst))
                                }
                            } else {
                                eprintln!("{} is not a valid addr!", words[2]);
                                std::process::exit(1);
                            }
                        }
                    }
                }
                "sb" => {
                    let instrinsic_mask = (5 as u64) << 57;
                    let dest = get_num_reg_from_asm(words[1]);
                    let src = words[2].parse::<u32>();
                    match src {
                        Ok(_v) => {
                            unreachable!()
                            //if words.len() == 4 {
                            //    let offset = words[3]
                            //        .parse::<u32>()
                            //        .expect(&format!("{} is not a valid offset!", words[3]));
                            //    let inst = instrinsic_mask
                            //        + ((dest as u64) << 54)
                            //        + v as u64
                            //        + offset as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //} else {
                            //    let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //}
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&words[2]) {
                                if words.len() == 4 {
                                    let offset = words[3]
                                        .parse::<u32>()
                                        .expect(&format!("{} is not a valid offset!", words[3]));
                                    let inst =
                                        instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                    inter_insts.push(HandleAsm::Other(inst));
                                } else {
                                    let inst = instrinsic_mask
                                        + ((dest as u64) << 54)
                                        + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                    inter_insts.push(HandleAsm::Other(inst))
                                }
                            } else {
                                eprintln!("{} is not a valid addr!", words[2]);
                                std::process::exit(1);
                            }
                        }
                    }
                }
                "sq" => {
                    let instrinsic_mask = (6 as u64) << 57;
                    let dest = get_num_reg_from_asm(words[1]);
                    let src = words[2].parse::<u32>();
                    match src {
                        Ok(_v) => {
                            unreachable!()
                            //if words.len() == 4 {
                            //    let offset = words[3]
                            //        .parse::<u32>()
                            //        .expect(&format!("{} is not a valid offset!", words[3]));
                            //    let inst = instrinsic_mask
                            //        + ((dest as u64) << 54)
                            //        + v as u64
                            //        + offset as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //} else {
                            //    let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //}
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&words[2]) {
                                if words.len() == 4 {
                                    let offset = words[3]
                                        .parse::<u32>()
                                        .expect(&format!("{} is not a valid offset!", words[3]));
                                    let inst =
                                        instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                    inter_insts.push(HandleAsm::Other(inst));
                                } else {
                                    let inst = instrinsic_mask
                                        + ((dest as u64) << 54)
                                        + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                    inter_insts.push(HandleAsm::Other(inst))
                                }
                            } else {
                                eprintln!("{} is not a valid addr!", words[2]);
                                std::process::exit(1);
                            }
                        }
                    }
                }
                "sh" => {
                    let instrinsic_mask = (7 as u64) << 57;
                    let dest = get_num_reg_from_asm(words[1]);
                    let src = words[2].parse::<u32>();
                    match src {
                        Ok(_v) => {
                            unreachable!()
                            //if words.len() == 4 {
                            //    let offset = words[3]
                            //        .parse::<u32>()
                            //        .expect(&format!("{} is not a valid offset!", words[3]));
                            //    let inst = instrinsic_mask
                            //        + ((dest as u64) << 54)
                            //        + v as u64
                            //        + offset as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //} else {
                            //    let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //}
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&words[2]) {
                                if words.len() == 4 {
                                    let offset = words[3]
                                        .parse::<u32>()
                                        .expect(&format!("{} is not a valid offset!", words[3]));
                                    let inst =
                                        instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                    inter_insts.push(HandleAsm::Other(inst));
                                } else {
                                    let inst = instrinsic_mask
                                        + ((dest as u64) << 54)
                                        + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                    inter_insts.push(HandleAsm::Other(inst))
                                }
                            } else {
                                eprintln!("{} is not a valid addr!", words[2]);
                                std::process::exit(1);
                            }
                        }
                    }
                }
                "sw" => {
                    let instrinsic_mask = (8 as u64) << 57;
                    let dest = get_num_reg_from_asm(words[1]);
                    let src = words[2].parse::<u32>();
                    match src {
                        Ok(_v) => {
                            unreachable!()
                            //if words.len() == 4 {
                            //    let offset = words[3]
                            //        .parse::<u32>()
                            //        .expect(&format!("{} is not a valid offset!", words[3]));
                            //    let inst = instrinsic_mask
                            //        + ((dest as u64) << 54)
                            //        + v as u64
                            //        + offset as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //} else {
                            //    let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            //    inter_insts.push(HandleAsm::Other(inst));
                            //}
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&words[2]) {
                                if words.len() == 4 {
                                    let offset = words[3]
                                        .parse::<u32>()
                                        .expect(&format!("{} is not a valid offset!", words[3]));
                                    let inst =
                                        instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                    inter_insts.push(HandleAsm::Other(inst));
                                } else {
                                    let inst = instrinsic_mask
                                        + ((dest as u64) << 54)
                                        + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                    inter_insts.push(HandleAsm::Other(inst))
                                }
                            } else {
                                eprintln!("{} is not a valid addr!", words[2]);
                                std::process::exit(1);
                            }
                        }
                    }
                }
                "pop" => {
                    let instrinsic_mask = (9 as u64) << 57;
                    let dest = get_num_reg_from_asm(words[1]);
                    let inst = instrinsic_mask + ((dest as u64) << 54);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "push" => {
                    let instrinsic_mask = (10 as u64) << 57;
                    let src1 = get_num_reg_from_asm(words[1]);
                    let inst = instrinsic_mask + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "ncall" => {
                    let instrinsic_mask = 13u64 << 57;
                    let addr = words[1];
                    match addr.parse::<u32>() {
                        Ok(v) => {
                            let inst = instrinsic_mask + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                        Err(_) => {
                            if let Some(v) = constants.get(addr) {
                                inter_insts.push(HandleAsm::Other(instrinsic_mask + *v as u64));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedCNT(
                                    instrinsic_mask,
                                    addr.to_owned(),
                                ));
                            }
                        }
                    }
                }
                "add" => {
                    let intrinsic_mask = (1u64 << 61) + (0 << 58) + (0 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "addi" => {
                    let intrinsic_mask = (1u64 << 61) + (0 << 58) + (1 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    if let Ok(imm) = words[3].parse::<i64>() {
                        let inst = intrinsic_mask
                            + ((dest as u64) << 54)
                            + ((src1 as u64) << 51)
                            + (imm as u64 & 4_294_967_295);
                        inter_insts.push(HandleAsm::Other(inst));
                    } else {
                        let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                        inter_insts.push(HandleAsm::UnresolvedCNT(inst, words[3].to_owned()));
                    }
                }
                "addf" => {
                    let intrinsic_mask = (2u64 << 61) + (0 << 58) + (1 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "sub" => {
                    let intrinsic_mask = (1u64 << 61) + (1 << 58) + (0 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "subi" => {
                    let intrinsic_mask = (1u64 << 61) + (1 << 58) + (1 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    if let Ok(imm) = words[3].parse::<i64>() {
                        let inst = intrinsic_mask
                            + ((dest as u64) << 54)
                            + ((src1 as u64) << 51)
                            + (imm as u64 & 4_294_967_295);
                        inter_insts.push(HandleAsm::Other(inst));
                    } else {
                        let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                        inter_insts.push(HandleAsm::UnresolvedCNT(inst, words[3].to_owned()));
                    }
                }
                "subf" => {
                    let intrinsic_mask = (2u64 << 61) + (1 << 58) + (1 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "mul" => {
                    let intrinsic_mask = (1u64 << 61) + (2 << 58) + (0 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "muli" => {
                    let intrinsic_mask = (1u64 << 61) + (2 << 58) + (1 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    if let Ok(imm) = words[3].parse::<i64>() {
                        let inst = intrinsic_mask
                            + ((dest as u64) << 54)
                            + ((src1 as u64) << 51)
                            + (imm as u64 & 4_294_967_295);
                        inter_insts.push(HandleAsm::Other(inst));
                    } else {
                        let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                        inter_insts.push(HandleAsm::UnresolvedCNT(inst, words[3].to_owned()));
                    }
                }
                "mulf" => {
                    let intrinsic_mask = (2u64 << 61) + (2 << 58) + (1 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "div" => {
                    let intrinsic_mask = (1u64 << 61) + (3 << 58) + (0 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "divi" => {
                    let intrinsic_mask = (1u64 << 61) + (3 << 58) + (1 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    if let Ok(imm) = words[3].parse::<i64>() {
                        let inst = intrinsic_mask
                            + ((dest as u64) << 54)
                            + ((src1 as u64) << 51)
                            + (imm as u64 & 4_294_967_295);
                        inter_insts.push(HandleAsm::Other(inst));
                    } else {
                        let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                        inter_insts.push(HandleAsm::UnresolvedCNT(inst, words[3].to_owned()));
                    }
                }
                "divf" => {
                    let intrinsic_mask = (2u64 << 61) + (3 << 58) + (1 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "mod" => {
                    let intrinsic_mask = (1u64 << 61) + (4 << 58) + (0 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "modi" => {
                    let intrinsic_mask = (1u64 << 61) + (4 << 58) + (1 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    if let Ok(imm) = words[3].parse::<i64>() {
                        let inst = intrinsic_mask
                            + ((dest as u64) << 54)
                            + ((src1 as u64) << 51)
                            + (imm as u64 & 4_294_967_295);
                        inter_insts.push(HandleAsm::Other(inst));
                    } else {
                        let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                        inter_insts.push(HandleAsm::UnresolvedCNT(inst, words[3].to_owned()));
                    }
                }
                "modf" => {
                    let intrinsic_mask = (2u64 << 61) + (4 << 58) + (1 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "itof_32" => {
                    let instrinsic_mask = (3u64 << 61) + (0 << 58);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let inst = instrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "itof_64" => {
                    let instrinsic_mask = (3u64 << 61) + (1 << 58);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let inst = instrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "ftoi_32" => {
                    let instrinsic_mask = (3u64 << 61) + (2 << 58);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let inst = instrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "ftoi_64" => {
                    let instrinsic_mask = (3u64 << 61) + (3 << 58);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let inst = instrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "eq_u" => {
                    let instrinsic_mask = (4u64 << 61) + (0 << 58);
                    let src1 = get_num_reg_from_asm(words[1]);
                    let src2 = get_num_reg_from_asm(words[2]);
                    let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "cmp_u" => {
                    let instrinsic_mask = (4u64 << 61) + (1 << 58);
                    let src1 = get_num_reg_from_asm(words[1]);
                    let src2 = get_num_reg_from_asm(words[2]);
                    let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "ez_u" => {
                    let instrinsic_mask = (4u64 << 61) + (2 << 58);
                    let src1 = get_num_reg_from_asm(words[1]);
                    let inst = instrinsic_mask + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "eq_i" => {
                    let instrinsic_mask = (4u64 << 61) + (1 << 57);
                    let src1 = get_num_reg_from_asm(words[1]);
                    let src2 = get_num_reg_from_asm(words[2]);
                    let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "cmp_i" => {
                    let instrinsic_mask = (4u64 << 61) + (3 << 57);
                    let src1 = get_num_reg_from_asm(words[1]);
                    let src2 = get_num_reg_from_asm(words[2]);
                    let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "ez_i" => {
                    let instrinsic_mask = (4u64 << 61) + (5 << 57);
                    let src1 = get_num_reg_from_asm(words[1]);
                    let inst = instrinsic_mask + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "size" => {
                    let instrinsic_mask = (4u64 << 61) + (6 << 57);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src = words[2].parse::<u32>();
                    match src {
                        Ok(v) => {
                            if words.len() == 4 {
                                let offset = words[3]
                                    .parse::<u32>()
                                    .expect(&format!("{} is not a valid offset!", words[3]));
                                let inst = instrinsic_mask
                                    + ((dest as u64) << 54)
                                    + v as u64
                                    + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            }
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&words[2]) {
                                if words.len() == 4 {
                                    let offset = words[3]
                                        .parse::<u32>()
                                        .expect(&format!("{} is not a valid offset!", words[3]));
                                    let inst =
                                        instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                    inter_insts.push(HandleAsm::Other(inst));
                                } else {
                                    let inst = instrinsic_mask
                                        + ((dest as u64) << 54)
                                        + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                    inter_insts.push(HandleAsm::Other(inst))
                                }
                            } else {
                                eprintln!("{} is not a valid addr!", words[2]);
                                std::process::exit(1);
                            }
                        }
                    }
                }
                "eq_f" => {
                    let instrinsic_mask = (5u64 << 61) + (0 << 58);
                    let src1 = get_num_reg_from_asm(words[1]);
                    let src2 = get_num_reg_from_asm(words[2]);
                    let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "cmp_f" => {
                    let instrinsic_mask = (5u64 << 61) + (1 << 58);
                    let src1 = get_num_reg_from_asm(words[1]);
                    let src2 = get_num_reg_from_asm(words[2]);
                    let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "ez_f" => {
                    let instrinsic_mask = (5u64 << 61) + (2 << 58);
                    let src1 = get_num_reg_from_asm(words[1]);
                    let inst = instrinsic_mask + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "hlt" => {
                    inter_insts.push(HandleAsm::Other(INST_HALT_VM));
                }
                "jmp" => {
                    let instrinsic_mask = (6u64 << 61) + (0 << 58);
                    let addr = words[1];
                    match addr.parse::<u32>() {
                        Ok(v) => {
                            let inst = instrinsic_mask + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&addr) {
                                let src1 = get_num_reg_from_asm(addr);
                                let inst = instrinsic_mask + ((src1 as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(
                                    instrinsic_mask,
                                    addr.to_owned(),
                                ));
                            }
                        }
                    }
                }
                "jez" => {
                    let instrinsic_mask = (6u64 << 61) + (1 << 58);
                    let addr = words[1];
                    match addr.parse::<u32>() {
                        Ok(v) => {
                            let inst = instrinsic_mask + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&addr) {
                                let src1 = get_num_reg_from_asm(addr);
                                let inst = instrinsic_mask + ((src1 as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(
                                    instrinsic_mask,
                                    addr.to_owned(),
                                ));
                            }
                        }
                    }
                }
                "jnz" => {
                    let instrinsic_mask = (6u64 << 61) + (2 << 58);
                    let addr = words[1];
                    match addr.parse::<u32>() {
                        Ok(v) => {
                            let inst = instrinsic_mask + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&addr) {
                                let src1 = get_num_reg_from_asm(addr);
                                let inst = instrinsic_mask + ((src1 as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(
                                    instrinsic_mask,
                                    addr.to_owned(),
                                ));
                            }
                        }
                    }
                }
                "jge" => {
                    let instrinsic_mask = (6u64 << 61) + (3 << 58);
                    let addr = words[1];
                    match addr.parse::<u32>() {
                        Ok(v) => {
                            let inst = instrinsic_mask + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&addr) {
                                let src1 = get_num_reg_from_asm(addr);
                                let inst = instrinsic_mask + ((src1 as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(
                                    instrinsic_mask,
                                    addr.to_owned(),
                                ));
                            }
                        }
                    }
                }
                "jlt" => {
                    let instrinsic_mask = (6u64 << 61) + (4 << 58);
                    let addr = words[1];
                    match addr.parse::<u32>() {
                        Ok(v) => {
                            let inst = instrinsic_mask + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&addr) {
                                let src1 = get_num_reg_from_asm(addr);
                                let inst = instrinsic_mask + ((src1 as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(
                                    instrinsic_mask,
                                    addr.to_owned(),
                                ));
                            }
                        }
                    }
                }
                "call" => {
                    let instrinsic_mask = (6u64 << 61) + (5 << 58);
                    let addr = words[1];
                    match addr.parse::<u32>() {
                        Ok(v) => {
                            let inst = instrinsic_mask + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                        Err(_) => {
                            if AVAIL_REGS.contains(&addr) {
                                let src1 = get_num_reg_from_asm(addr);
                                let inst = instrinsic_mask + ((src1 as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(
                                    instrinsic_mask,
                                    addr.to_owned(),
                                ));
                            }
                        }
                    }
                }
                "ret" => {
                    let instrinsic_mask = (6u64 << 61) + (7 << 58);
                    inter_insts.push(HandleAsm::Other(instrinsic_mask));
                }
                "and" => {
                    let intrinsic_mask = (7u64 << 61) + (0 << 58);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "or" => {
                    let intrinsic_mask = (7u64 << 61) + (1 << 58);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "xor" => {
                    let intrinsic_mask = (7u64 << 61) + (2 << 58);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "shl" => {
                    let intrinsic_mask = (7u64 << 61) + (4 << 58);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }
                "shr" => {
                    let intrinsic_mask = (7u64 << 61) + (5 << 58);
                    let dest = get_num_reg_from_asm(words[1]);
                    let src1 = get_num_reg_from_asm(words[2]);
                    let src2 = get_num_reg_from_asm(words[3]);
                    let inst = intrinsic_mask
                        + ((dest as u64) << 54)
                        + ((src1 as u64) << 51)
                        + ((src2 as u64) << 48);
                    inter_insts.push(HandleAsm::Other(inst));
                }

                _ => self.codegen_ext(words, &mut labels, &mut constants, &mut inter_insts),
            }
        }
        let without_data_directives = inter_insts.iter().filter(|x| match x {
            HandleAsm::UnresolvedRef(_) => true,
            _ => false,
        }).count();
        let mut prog_size = (inter_insts.len() - without_data_directives) * 8 + 8;
        let mut consts_resolve : Vec<(String, usize)> = constants.iter().map(|x| (x.0.clone(),*x.1)).collect();
        consts_resolve.sort_by(|a,b| a.1.cmp(&b.1));
        let mut data_blocks = Vec::new();
        for name in consts_resolve.iter().map(|x| x.0.clone()) {
           for check in consts_resolve.iter().map(|x| x.0.clone()) {
                if name == format!("{check}::size") {
                    data_blocks.push(check);
                    break;
                }
            } 
        }
        let data_blocks = consts_resolve.iter().filter(|x| data_blocks.contains(&x.0)).collect::<Vec<&(String,usize)>>();
        for datum in data_blocks {
            let size = format!("{}::size",&datum.0);
            if let Some (actual_size) = constants.get(&size) {
                let actual_size = *actual_size;
                if let Some(proper_entry) = constants.get_mut(&datum.0) {
                    *proper_entry = prog_size;
                    prog_size += actual_size;
                }
            }
        }
        //let insts: Vec<u64> = inter_insts.into_iter().map(HandleAsm::unwrap).collect();
        
        let mut data_section = Vec::new();
        prog_size = inter_insts.len() * 8 + 8;
        let mut insts: Vec<u64> = vec![];
        for (i, inst) in inter_insts.iter().enumerate() {
            match inst {
                HandleAsm::Other(a) => insts.push(*a),
                HandleAsm::UnresolvedJMP(a, addr) => {
                    if let Some(to_jmp) = labels.get(addr) {
                        let reljmp: i64 = *to_jmp as i64 - i as i64;
                        let inst = a + ((reljmp * 8) & 4_294_967_295) as u64;
                        insts.push(inst);
                    } else if let Some(cons) = constants.get(addr) {
                        insts.push(a + *cons as u64);
                    } else {
                        eprintln!("Undefined Label: {}", addr);
                        std::process::exit(1);
                    }
                }
                HandleAsm::UnresolvedCNT(a, addr) => {
                    if let Some(value) = labels.get(addr) {
                        let inst = a + *value as u64;
                        insts.push(inst);
                    } else if let Some(cons) = constants.get(addr) {
                        insts.push(a + *cons as u64);
                    } else {
                        eprintln!("Undefined Label: {}", addr);
                        std::process::exit(1);
                    }
                }
                HandleAsm::UnresolvedRef(bites) => {
                    //insts.push(0);
                    prog_size += bites.len();
                    data_section.push(bites);
                }
            }
        }
        for elem in insts {
            let bytes = elem.to_le_bytes();
            for byte in bytes {
                ret.push(byte)
            }
        }
        for b in MKNBDATA {
            ret.push(b);
        }
        for data in data_section {
            for b in data {
                ret.push(*b);
            }
        }
        let mut map_file = Vec::new();
        let mut lbls = labels
            .into_iter()
            .map(|mut x| {
                x.1 *= 8;
                x
            })
            .collect();
        let mut consts = constants.into_iter().collect();
        map_file.append(&mut lbls);
        map_file.append(&mut consts);
        (ret, map_file)
    }
}
