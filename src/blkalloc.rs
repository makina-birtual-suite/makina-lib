use crate::{MAX_PAGES, PAGE_SIZE};
use std::ops::Index;
use std::ops::Range;
use std::sync::Arc;
use std::sync::RwLock;
type Page = Arc<RwLock<[u8; PAGE_SIZE as usize]>>;

pub trait Indexer<I, O> {
    fn read_at(&self, index: I) -> O;
    fn write_at(&self, index: I, value: O);
    fn write_all(&self, index: I, value: &[O]);
    fn read_all(&self, index: Range<I>, buffer: &mut [O]);
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
struct AllocMetaData {
    begin: u64,
    size: u64,
}

impl AllocMetaData {
    fn new(begin: u64, size: u64) -> Self {
        Self { begin, size }
    }
}

#[derive(Debug)]
pub struct BlockAllocator {
    used: Vec<AllocMetaData>,
    free: Vec<AllocMetaData>,
    mem: Vec<Page>,
    pagenum: u64,
}

impl BlockAllocator {
    pub fn new() -> Self {
        Self {
            used: Vec::new(),
            free: Vec::new(),
            mem: Vec::new(),
            pagenum: 0,
        }
    }
    pub fn mmap(&mut self, npages: u64) {
        for _ in 0..npages {
            let page = Arc::new(RwLock::new([0; PAGE_SIZE as usize]));
            self.mem.push(page);
        }
    }
    pub fn mem_alloc(&mut self, nbyte: u64) -> u64 {
        //self.free.sort_by(|a, b| a.begin.cmp(&b.begin));
        self.squash_blocks(true);
        for (i, block) in self.free.iter_mut().enumerate() {
            if block.size >= nbyte {
                let mut new_block = *block;
                new_block.size = nbyte;
                block.begin += nbyte;
                block.size -= nbyte;
                if block.size == 0 {
                    self.free.swap_remove(i);
                }
                self.used.push(new_block);
                return new_block.begin;
            }
        }
        let needed_pages = (nbyte / PAGE_SIZE) + 1;
        self.free.push(AllocMetaData::new(
            self.pagenum * PAGE_SIZE,
            needed_pages * PAGE_SIZE,
        ));
        self.pagenum += needed_pages;
        self.mmap(needed_pages);
        return self.mem_alloc(nbyte);
    }
    pub fn mem_free(&mut self, ptr: u64) {
        self.used.sort_by(|a, b| a.begin.cmp(&b.begin));
        for (i, block) in self.used.iter_mut().enumerate() {
            if block.begin <= ptr && (block.size + block.begin) > ptr {
                self.free.push(self.used.swap_remove(i));
                break;
            }
        }
        if self.pagenum > MAX_PAGES {
            self.squash_blocks(false);
        }
    }
    pub fn mem_realloc(&mut self, ptr: u64, size: u64) -> u64 {
        self.used.sort_by(|a, b| a.begin.cmp(&b.begin));
        for block in self.used.iter_mut() {
            if block.begin <= ptr && block.size + block.begin > ptr {
                let begin = block.begin;
                let siblk = block.size;
                self.mem_free(begin);
                let new_reg = self.mem_alloc(size);
                if new_reg != begin {
                    let mut buffer = [0u8;siblk.min(size)];
                    self.read_all(begin..begin + buffer.len(), &mut buffer);
                    self.write_all(new_reg, &buffer);
                }
                return new_reg;
            }
        }
        panic!("Attempted to realloc an invalid memory region (use after free?)");
    }

    fn squash_blocks(&mut self, called_by_malloc: bool) {
        if self.free.len() == 0 {
            return;
        }
        self.free.sort_by(|a, b| a.begin.cmp(&b.begin));
        for i in 0..(self.free.len() - 1) {
            let next = self.free.get(i + 1).map(|x| *x);
            if let (Some(fst), Some(snd)) = (self.free.get_mut(i), next) {
                if (fst.begin + fst.size) == snd.begin {
                    fst.size += snd.size;
                    self.free.swap_remove(i + 1);
                    self.squash_blocks(called_by_malloc);
                    return;
                }
            }
        }
        if self.pagenum > MAX_PAGES && !called_by_malloc {
            self.used.sort_by(|a, b| a.begin.cmp(&b.begin));
            if self.used.len() > 0 {
                let last_used = self.used.last().unwrap();
                let mut last_used_page = ((last_used.begin + last_used.size) / PAGE_SIZE) + 1;
                if last_used_page < (MAX_PAGES + 1) {
                    last_used_page = MAX_PAGES + 1;
                }
                while (last_used_page + 1) < self.pagenum {
                    self.mem.pop();
                    self.pagenum -= 1;
                    if let Some(mut l) = self.free.pop() {
                        if l.size >= PAGE_SIZE {
                            l.size -= PAGE_SIZE;
                            if l.size > 0 {
                                self.free.push(l);
                            }
                        } else {
                            unreachable!("As we free from the end, it should not be possible that there exists such free block that isn't contiguous in the end");
                        }
                    }
                }
            } else {
                let used_lenght = self.pagenum;
                for _ in MAX_PAGES..used_lenght {
                    self.mem.pop();
                }
                self.pagenum = MAX_PAGES;
                self.free = vec![AllocMetaData::new(0, MAX_PAGES * PAGE_SIZE)];
            }
        }
    }
    /// Finds the block of which the pointer belongs, and returns the distance until the end of the
    /// block as the size.
    pub fn size_of_alloc(&self, ptr: u64) -> u64 {
        for block in self.used.iter() {
            if block.begin <= ptr && block.size + block.begin > ptr {
                return block.begin + block.size - ptr;
            }
        }
        panic!("Tried to find the size of a non existing allocation! (use after free?)")
    }
    pub fn parent_of_alloc(&self, ptr: u64) -> u64 {
        for block in self.used.iter() {
            if block.begin <= ptr && block.size + block.begin > ptr {
                return block.begin;
            }
        }
        panic!("Tried to find the size of a non existing allocation! (use after free?)")
    }
}

impl Indexer<usize, u8> for BlockAllocator {
    fn read_at(&self, index: usize) -> u8 {
        let page = index / PAGE_SIZE as usize;
        let offset = index % PAGE_SIZE as usize;
        let mem = self.mem[page].read().unwrap();
        mem[offset]
    }
    fn write_at(&self, index: usize, value: u8) {
        let page = index / PAGE_SIZE as usize;
        let offset = index % PAGE_SIZE as usize;
        let mut mem = self.mem[page].write().unwrap();
        mem[offset] = value;
    }
    fn write_all(&self, index: usize, value: &[u8]) {
        let mut page = index / PAGE_SIZE as usize;
        let mut mem = self.mem[page].write().unwrap();
        for (i, v) in value.iter().enumerate() {
            let offset = (index + i) % PAGE_SIZE as usize;
            let new_page = (index + i) / PAGE_SIZE as usize;
            if new_page != page {
                mem = self.mem[new_page].write().unwrap();
                page = new_page;
            }
            mem[offset] = *v;
        }
    }
    fn read_all(&self, index: Range<usize>, buffer: &mut [u8]) {
        let mut smol = index.start;
        let mut page = smol / PAGE_SIZE as usize;
        let mut mem = self.mem[page].read().unwrap();
        for i in index {
            let offset = i % PAGE_SIZE as usize;
            let new_page = i / PAGE_SIZE as usize;
            if new_page != page {
                mem = self.mem[new_page].read().unwrap();
                page = new_page;
            }
            buffer[i - smol] = mem[offset];
        }
    }
}

impl crate::AllocatorInterface for BlockAllocator {
    fn mfree(&mut self, ptr: u64) {
        self.mem_free(ptr);
    }
    fn malloc(&mut self, nbytes: u64) -> u64 {
        self.mem_alloc(nbytes)
    }
    fn mrealloc(&mut self, ptr: u64, size: u64) -> u64 {
        self.mem_realloc(ptr, size)
    }
    fn sizealloc(&self, ptr: u64) -> u64 {
        self.size_of_alloc(ptr)
    }
    fn parentalloc(&self, ptr: u64) -> u64 {
        self.parent_of_alloc(ptr)
    }
}

#[cfg(test)]
mod blk_tests {
    use crate::blkalloc::AllocMetaData;
    use crate::blkalloc::BlockAllocator;
    use crate::PAGE_SIZE;
    #[test]
    fn alloc_once() {
        let mut al = BlockAllocator::new();
        let ptr = al.mem_alloc(100);
        println!("{:?}", &al);
        if al.mem.len() == 0 {
            assert!(false, "After an allocation, pages cannot be nil");
        }
        assert!(
            al.pagenum == 1,
            "After allocating 100 bytes, only one page should be allocated"
        );
        assert!(
            al.used.len() == 1,
            "After allocating once, there should be one block in the used pool"
        );
        let metadatum = al.used[0];
        assert_eq!(metadatum, AllocMetaData::new(0, 100));
        let metadatum = al.free[0];
        assert_eq!(metadatum, AllocMetaData::new(100, PAGE_SIZE - 100));
        al.mem_free(ptr);
        assert_eq!(al.used.clone(), vec![]);
        assert_eq!(
            al.free.clone(),
            vec![
                AllocMetaData::new(100, PAGE_SIZE - 100),
                AllocMetaData::new(0, 100)
            ]
        );
    }
    #[test]
    fn alloc_multiple() {
        let mut allocator = BlockAllocator::new();
        const FST_ALLOC_SIZE: u64 = 100;
        let mut resulting_ptrs = Vec::new();
        for _ in 0..1000 {
            //println!("{}",allocator.pagenum);
            resulting_ptrs.push(allocator.mem_alloc(FST_ALLOC_SIZE));
        }
        println!("First allocation works!");
        assert_eq!(allocator.used.len(), 1000);
        assert_eq!(allocator.free.len(), 1);
        for i in 0..1000 {
            if i % 2 == 0 {
                allocator.mem_free(resulting_ptrs[i]);
            }
        }
        assert_eq!(allocator.used.len(), 500);
        assert_eq!(allocator.free.len(), 501);
        for _ in 0..1000 {
            allocator.mem_alloc(FST_ALLOC_SIZE / 2);
        }
        assert_eq!(allocator.used.len(), 1500);
        assert_eq!(allocator.free.len(), 1);
    }
}
